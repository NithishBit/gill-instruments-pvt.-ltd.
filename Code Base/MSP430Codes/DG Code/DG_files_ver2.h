//---------------------------FLAG[15]------------------------------------------
#define DollarStart 0 //+/
#define WFlag 1  //+/
#define WifiBlink 2 //+/
#define RstFlag 3 //+/
#define Pub_OK 4 //+/
#define WifiConnect 5 //+/
#define Fram_WR  6 // License active flag
#define Auto_blink 7 //+/
#define Notification 8 //+/
#define Eb_Current_status 9 //+/
#define Eb_Previous_state 10 //+/
#define Buzzer_Blink 11 //+/
#define Buzzer_Blink2 12
#define Buzzer_CNT_EN 13
#define Buzzer_Mute 14

//-------------------- LED -----------------------------------------------------
#define DG_STATUS BIT0  //DG_STATUS for DG status
#define WiFi_LED BIT6 //WiFi_LED for WiFi
#define AUTO_LED BIT7 //AUTO_LED for AUTO MODE
//----------------FRAM Adress---------------------------------------------------
#define FRAM_DG_ADDRESS 0x1800
#define FRAM_EB_ADDress 0x1840
#define FRAM_ACTIVE 0x1880


bool FLAG[15] = {0};
char uart1RxBuff[60]={0};
short int licActive=0,EB_DG_Timer=0,modeFlag=2,EB_DG_status=0,status_change=0,Notify=0; //short for 0,1,2,3
short int Buzzer_Counter=0,DG_Fail=0;
int WDTimer=0,Power_timer=0,DG_Timer=0;
int rxBuffIndex=0,e1Counter=0,PS_Count=0,psCount=0,wd_count=0,DollarCount=0;
unsigned long long *Flash_ptrDG;
unsigned long long *Flash_ptrPower;
unsigned long *Flash_ptrACTIVE;      // Harware Lic Active
unsigned long long DG_Minute,POWER_Minute;

//----------------------Function Declaration------------------------------------
void Delay(void)
{
  for(long int i=65500;i>0;(i--))
  {
    __delay_cycles(10);
  }
}

void Delay_3X(void)
{
  Delay();
  Delay();
  Delay();
  Delay();
}
//------------------------------------------------------------------------------
void power_up()
{
 P1OUT|= (DG_STATUS+AUTO_LED+WiFi_LED);
 Delay();
 P1OUT&=~(DG_STATUS+AUTO_LED+WiFi_LED);
}
//------------------------------------------------------------------------------

void ResetWifi(void)
{ 
  Delay(); 
  P3OUT |= BIT1;
  Delay_3X();
  P3OUT &= ~BIT1;
  Delay_3X(); 
  P3OUT |= BIT1;
  P1OUT&=~WiFi_LED;
}

//--------------------------- Flash write --------------------------------------
void writeActive (short licActive)
{
  SYSCFG0 = FRWPPW | PFWP;
  *Flash_ptrACTIVE= licActive;                   // Write value to flash
  SYSCFG0 = FRWPPW | PFWP | DFWP;
}

void write_DG (unsigned long long DG_Minute)
{
  SYSCFG0 = FRWPPW | PFWP;
  *Flash_ptrDG= DG_Minute;                   // Write value to flash
  SYSCFG0 = FRWPPW | PFWP | DFWP;
}

void write_Power (unsigned long long POWER_Minute)
{
  SYSCFG0 = FRWPPW | PFWP;
  *Flash_ptrPower = POWER_Minute;                   // Write value to flash
  SYSCFG0 = FRWPPW | PFWP | DFWP;
}

//==============================================================================
void Tx_Uart(char * str)
{
  int i = 0;
  for(i = 0; i < strlen(str); i++)
  {
    while(!(UCA1IFG & UCTXIFG));
    if(licActive!=1)
    UCA1TXBUF = str[i];
  }
}

//------------------------------------------------------------------------------
void TxCloud(void)
{
  if(FLAG[WifiConnect]==1)
{  
  char DG_buf[40]={0};
  FLAG[Pub_OK]=0;
  snprintf(DG_buf,40,"$%d;%lld;%lld;0;%d!",EB_DG_status,POWER_Minute,DG_Minute,modeFlag);        //XX;Y;Z;@  modeFlag ntfy_flag
  Tx_Uart(DG_buf);
  PS_Count=0;
   }
}
void TxCloudNotify(void)
{
  if(FLAG[WifiConnect]==1)
{  
  char DG_buf[40]={0};
  FLAG[Pub_OK]=0;
  snprintf(DG_buf,40,"$%d;%lld;%lld;%d;%d!",EB_DG_status,POWER_Minute,DG_Minute,Notify,modeFlag);   //XX;Y;Z;@  modeFlag ntfy_flag
  Tx_Uart(DG_buf);
  PS_Count=0;Notify=0;
   }
}
//-----------------------Configure UART ----------------------------------------
void UartOneInit(void)
{
  UCA1CTLW0 |= UCSWRST;                                                         // Baud Rate calculation
  UCA1CTLW0 |= UCSSEL__SMCLK;                                                    // 8000000/(16*9600) = 52.083
  UCA1BR0 = 52;                                                                 // Fractional portion = 0.083
  UCA1BR1 = 0x00;                                                               // User's Guide Table 14-4: UCBRSx = 0x49
  UCA1MCTLW = 0x4900 | UCOS16 | UCBRF_1;                                        // UCBRFx = int ( (52.083-52)*16) = 1
  UCA1CTLW0 &= ~UCSWRST;                                                        // Initialize eUSCI
  UCA1IE |= UCRXIE;                                                             // Enable USCI_A0 RX interrupt
}
//-------------------------- DCO -----------------------------------------------
void DcoConfig(void)
{
  __bis_SR_register(SCG0);                                                      // disable FLL
  CSCTL3 |= SELREF__REFOCLK;                                                    // Set REFO as FLL reference source
  CSCTL0 = 0;                                                                   // clear DCO and MOD registers
  CSCTL1 &= ~(DCORSEL_7);                                                       // Clear DCO frequency select bits first
  CSCTL1 |= DCORSEL_3;                                                          // Set DCO = 8MHz
  CSCTL2 = FLLD_0 + 243;                                                        // DCODIV = 8MHz
  __delay_cycles(3);
  __bic_SR_register(SCG0);                                                      // enable FLL
  while(CSCTL7 & (FLLUNLOCK0 | FLLUNLOCK1));                                    // Poll until FLL is locked
// set default REFO(~32768Hz) as ACLK source, ACLK = 32768Hz
  CSCTL4 = SELMS__DCOCLKDIV | SELA__REFOCLK;                                    // default DCODIV as MCLK and SMCLK source  
}
//------------------------------------------------------------------------------
void Init_GPIO(void)
{
  P1DIR &= ~(BIT1+BIT2); P1DIR |= DG_STATUS+AUTO_LED+WiFi_LED;                            
  P3DIR |= BIT0+BIT1+BIT2; 
  P2DIR |= BIT4;  P2SEL0 |= BIT5 | BIT6;                                                        // set 2-UART pin as second function
  P1OUT = 0;  P2OUT = 0;  P3OUT = 0;
  P3OUT |= BIT1;
  P2OUT |= BIT3;                                                                // Configure P2.3 as pulled-up
  P2REN |= BIT3;                                                                // P2.3 pull-up register enable
  P2IES |= BIT3;                                                                // P2.3 Hi/Low edge
  P2IE |= BIT3;                                                                 // P2.3 interrupt enabled
  PM5CTL0 &= ~LOCKLPM5;                                                         // Disable the GPIO power-on default high-impedance mode
}