//******************************************************************************
//  MSP430FR2632 : DG SET POWER Switching using Single phase preventer.
//
//  Description: //XX;Y;Z;@ CON,COFF , W,SP $$ bUZZER
//
//                MSP430FR2633
//             -----------------
//         /|\|             P1.0|-->LED/D5/DG Status
//          | |             P1.1|<-- 1-Phase Preventer (EB)
//            |             P1.2|<-- 1-Phase Preventer (DG)
//          --|RST          P1.6|-->LED/D2 (BESCOM Power)  
//            |             P1.7|-->LED/D3 (Wifi) 
//            |             P2.3|<--SW
//            |             P2.4|-->SW_LED 
//            |             P3.0|-->Relay (DG Start)
//            |             P3.1|-->EN/ESP32  
//            |       (RXD1)P2.5|<--TXD0/ESP32
//            |       (TXD1)P2.6|-->RXD0/ESP32 
//            |_________________|
//
// EB_DG_status=0 -->No Power
// EB_DG_status=1 -->BESCOM POWER
// EB_DG_status=2 -->DG POWER
// EB_DG_status=3 -->Both BESCOM & DG POWER is ON
//
// EB_DG_Timer =0 -->Timer ON for EB
// EB_DG_Timer =1 -->Timer ON for DG
// EB_DG_Timer =2 -->Timer for No DG No EB
// EB_DG_Timer =3 -->Timer for both ON
//
// Notify=1-->Power Fail
// Notify=2-->Power UP 
// Notify=3-->Emergency Stop!
// Notify=4-->Emergency Start!
// Notify=5-->Temperature High!
// Notify=6-->Fuel Low!
// Notify=7-->Fuel Theft!
// Notify=8-->DG Failed to start!
// Notify=9-->DG Failed to stop!
//
// master switch Mode selection
// modeFlag=0--> OFF
// modeFlag=1--> ON
// modeFlag=2--> AUTO
// modeFlag=4--> Emergency stop
//
//  Pradhan V
//  Gill Instruments,pvt ltd
//  10 Oct 2018
//  IAR Embedded Workbench Version: 6.50
//******************************************************************************

#include <msp430fr2633.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "DG_files_ver2.h"

/////////////////////////////////////////////////////////////////////////////////
main(void)
{
  WDTCTL = WDT_ADLY_1000;                                                       // 1000ms
  SFRIE1 |= WDTIE;                                                              // Enable WDT interrupt
  Init_GPIO();
  DcoConfig();                   
  UartOneInit();
//------------------------------------------------------------------------------
  power_up(); ResetWifi(); FLAG[Auto_blink]=1;
  Flash_ptrDG = (unsigned long long *)FRAM_DG_ADDRESS; 
  Flash_ptrPower= (unsigned long long *)FRAM_EB_ADDress; 
  DG_Minute=*Flash_ptrDG;
  POWER_Minute=*Flash_ptrPower;
  P2IFG &= ~BIT3;                                                               // P2.3 IFG cleared
//------------------------------------------------------------------------------  
  while (1)                                 
  {
    if(DG_Timer>59)
    {
      DG_Timer=DG_Timer-60;            // write in Flash 
      write_DG(++DG_Minute);                    
    }
    if(Power_timer>59)
    {
      Power_timer=Power_timer-60;
      write_Power(++POWER_Minute);      // write in Flash              
    }
//---------------------- Switch ------------------------------------------------
    int temp = P1IN & 0x06;
    switch(temp)
    {
    case 0x02:  // NO EB Power but DG is ON
      FLAG[Eb_Current_status]=0; 
      EB_DG_Timer=1; 
      EB_DG_status =2; 
      //if(modeFlag==2)
      //Notify=1;                  //-->Power Fail                 
      P1OUT |= DG_STATUS;  
      __bis_SR_register(LPM3_bits|GIE);                                     // Enter LPM3 w/interrupt
      __no_operation();               
      break;
    case 0x04:                                     // No DG but EB Power is ON
      FLAG[Eb_Current_status]=1;
      EB_DG_Timer=0;
      EB_DG_status =1;
      //if(modeFlag==2)
     // Notify=2;                 //-->Power UP 
      P1OUT &= ~DG_STATUS;  
      __bis_SR_register(LPM3_bits|GIE);                                     // Enter LPM3 w/interrupt
      __no_operation();
      break;
    case 0x06:                                          // Both EB & DG is OFF
      FLAG[Eb_Current_status]=0;
      EB_DG_Timer=2; 
      EB_DG_status =0; 
     // Notify=1;                  //-->Power Fail
      P1OUT &= ~DG_STATUS; 
      __bis_SR_register(LPM3_bits|GIE);                                     // Enter LPM3 w/interrupt
      __no_operation();
      break; 
    case 0x00:                                           // Both EB & DG is ON 
      FLAG[Eb_Current_status]=1; 
      EB_DG_Timer=3; 
      EB_DG_status = 3;
      //Notify=2;                 //-->Power UP 
      P1OUT |= DG_STATUS;  
      __bis_SR_register(LPM3_bits|GIE); 
      __no_operation();
      break;
    default:  
      break;
    }
//------------------------------------------------------------------------------      
    if(FLAG[Fram_WR]==1)
    {
      writeActive(licActive);  
      FLAG[Fram_WR]=0;
    }  
    if((FLAG[RstFlag]==true)||(e1Counter>120))
    {
      ResetWifi(); 
      FLAG[RstFlag]=0;
      e1Counter=0;
    }
    if((wd_count >300)||(FLAG[WFlag]==1)) // 300=5min
    {
      TxCloud();                    // status,POWER_Minute,DG_Minute,modeFlag 
      wd_count=0;FLAG[WFlag]=0;
    }
//-----------------------------------------------------------------------------  
    if(status_change!=EB_DG_status)
    {
      FLAG[Buzzer_Blink]=0; 
      FLAG[Buzzer_Mute]=0; 
      WDTimer=0;
      TxCloudNotify();
    }
    status_change=EB_DG_status;
//-----------------------------------------------------------------------------     
  }
}
//*************************** END OF MAIN **************************************


//------------------------------------------------------------------------------
// Watchdog Timer interrupt service routine
//------------------------------------------------------------------------------
 #pragma vector=WDT_VECTOR
__interrupt void watchdog_timer(void)

{
  WDTimer++;wd_count++;
  if(EB_DG_Timer==1)        
  {
    DG_Timer++; // DG on
  }
  else if(EB_DG_Timer==0)
  {
    Power_timer++;// EB on
  }       
  else if(EB_DG_Timer==3)
  {
    Power_timer++;  // Both on
    DG_Timer++;
  }   
  
  if((FLAG[Eb_Current_status]==0)&&(WDTimer==30)&&(modeFlag==2))
  {
    P3OUT |= BIT0;
    Notify=1;                  //-->Power Fail
    TxCloudNotify();
    FLAG[Buzzer_CNT_EN]=1;
    FLAG[Buzzer_Blink]=1;
    Buzzer_Counter =0;
  }
  if((FLAG[Eb_Current_status]==1)&&(WDTimer==30)&&(modeFlag==2))
  {
    P3OUT &= ~BIT0; 
    Notify=2;                 //-->Power UP 
    TxCloudNotify();
    FLAG[Buzzer_CNT_EN]=1;
    FLAG[Buzzer_Blink]=1;
    Buzzer_Counter =0;
  }
  
  if((FLAG[Buzzer_CNT_EN]==1))
    Buzzer_Counter++;
  
  if((FLAG[Buzzer_Blink]==1)&&(Buzzer_Counter>9))
  {
    P3OUT ^= BIT2;
    FLAG[Buzzer_CNT_EN]=0;
  }

//------------------------------------------------------------------------------
  if(FLAG[WifiConnect]==true)
  {
    e1Counter =0;
    PS_Count++; 
    P1OUT|= WiFi_LED;   
  }
  else
  {
    e1Counter++;
  }
  
  if(FLAG[Auto_blink]==1)
  { 
    P1OUT ^= AUTO_LED;
  }
  if(FLAG[WifiBlink]==1)
  {
    P1OUT ^= WiFi_LED;
  }
//------------------------------------------------------------------------------
  if((PS_Count==10)&&(FLAG[Pub_OK]!=1))
  {
    FLAG[WFlag] =1;  PS_Count=0; psCount++;
  }
  if(psCount>3)
  {
    FLAG[RstFlag]=1; 
    psCount=0;
    FLAG[WifiConnect]=0;
  }
  if(FLAG[Pub_OK]==1)
  {
    psCount=0;
    PS_Count=0;
  }
//------------------------------------------------------------------------------
  switch(modeFlag)
  {
   
  case 0://   DG mode -> off state from Mobile
    if((EB_DG_status==3||EB_DG_status==2)&&(FLAG[Buzzer_Mute]!=1))
    {
      FLAG[Buzzer_CNT_EN]=1;
      FLAG[Buzzer_Blink]=1;
      if((FLAG[Buzzer_Blink]==1)&&(Buzzer_Counter>9)&&(DG_Fail==0))
      {
        DG_Fail=1;
        Notify=9;             //-->DG Failed to stop
      status_change=0;
      }
    }
    else if((EB_DG_status==0||EB_DG_status==1))
    {
      P3OUT &= ~BIT2;
      Buzzer_Counter =0; 
      P3OUT &= ~BIT0;DG_Fail=0;
      FLAG[Buzzer_Mute]=1;
    }
    else 
    {
      FLAG[Buzzer_CNT_EN]=0;
      Buzzer_Counter =0;
      P3OUT &= ~BIT2;
    }
    break;
//------------------------------------------------------------------------------
  case 1:  // DG mode -> ON state by mobile or switch
    if((EB_DG_status==0||EB_DG_status==1)&&(FLAG[Buzzer_Mute]!=1))
    {
      FLAG[Buzzer_CNT_EN]=1;
      FLAG[Buzzer_Blink]=1;
      if((FLAG[Buzzer_Blink]==1)&&(Buzzer_Counter>9)&&(DG_Fail==0))
      {
        DG_Fail=1;
        Notify=8;         //-->DG Failed to start
      status_change=0;
      }
    }
    else if((EB_DG_status==2||EB_DG_status==3))
    {
      P3OUT &= ~BIT2;
      Buzzer_Counter =0;DG_Fail=0;
      FLAG[Buzzer_Mute]=1;
    }
    else 
    {
      FLAG[Buzzer_CNT_EN]=0;
      Buzzer_Counter =0;
      P3OUT &= ~BIT2; 
    }
    break;
//------------------------------------------------------------------------------
  case 2:// DG mode -> Auto state by mobile or switch
    if((EB_DG_status==0)&&(FLAG[Buzzer_Mute]!=1))
    {    
       if((FLAG[Buzzer_Blink]==1)&&(Buzzer_Counter>9)&&(DG_Fail==0))
      {
        DG_Fail=1;         
      
        Notify=8;         //-->DG Failed to start
      TxCloudNotify();
      }
    }
    else if((EB_DG_status==3)&&(FLAG[Buzzer_Mute]!=1))
    {
      if((FLAG[Buzzer_Blink]==1)&&(Buzzer_Counter>9)&&(DG_Fail==0))
      {
        DG_Fail=1;         
      Notify=9;             //-->DG Failed to stop
     TxCloudNotify();
    }
     
    }
     else if((EB_DG_status==2||EB_DG_status==1))
    {
      P3OUT &= ~BIT2;FLAG[Buzzer_CNT_EN]=0;
      Buzzer_Counter =0;DG_Fail=0;
      FLAG[Buzzer_Mute]=1;
    }

    break;
//------------------------------------------------------------------------------
  case 4:// DG mode -> OFF state (emrgency stop by switch)
    if((EB_DG_status==3||EB_DG_status==2)&&(FLAG[Buzzer_Mute]!=1))
    {
      FLAG[Buzzer_CNT_EN]=1;
      FLAG[Buzzer_Blink]=1; 
      if((FLAG[Buzzer_Blink]==1)&&(Buzzer_Counter>9)&&(DG_Fail==0))
      {
        DG_Fail=1;
        Notify=9;             //-->DG Failed to stop
      status_change=0;
    }
    }
    else if((EB_DG_status==0||EB_DG_status==1))
    {
      P3OUT &= ~BIT2;
      Buzzer_Counter =0;
      P3OUT &= ~BIT0;DG_Fail=0;
      FLAG[Buzzer_Mute]=1;
    }
    else 
    {
      FLAG[Buzzer_CNT_EN]=0;
      Buzzer_Counter =0;
      P3OUT &= ~BIT2;
    }
    break;  
  default:
    break;
  }
  __bic_SR_register_on_exit(LPM3_bits);             // Clear LPM3 bits from 0(SR)
}

//------------------------------------------------------------------------------
// Port 2 interrupt service routine // Master Switch
//------------------------------------------------------------------------------

#pragma vector=PORT2_VECTOR
__interrupt void Port_2(void)
{
  if((FLAG[Buzzer_Blink]==1)&&(Buzzer_Counter>9))
  {
    P3OUT &= ~BIT2;              // Mute Logic
    Buzzer_Counter =0;
    P3OUT &= ~BIT0;
    FLAG[Buzzer_Mute]=1;
  }
  else
  {
    if(modeFlag==2)
    {
      modeFlag=1;                  // This code is exc for ON mode 
      P3OUT |= BIT0;  
      P1OUT |= AUTO_LED;
      FLAG[Auto_blink]=0;
      //FLAG[WFlag] =1;
      FLAG[Buzzer_Mute]=0;
      Buzzer_Counter =0;
      Notify=4; //Emergency Start
      status_change=0;
    }
    else if(modeFlag==1)
    {
      modeFlag=4;                   // This code is exc for OFF mode 
      P3OUT &= ~BIT0;
      P1OUT &= ~AUTO_LED;
      FLAG[Auto_blink]=0;
      //FLAG[WFlag] =1;
      FLAG[Buzzer_Mute]=0;
      Buzzer_Counter =0;
      Notify=3;  //Emergency Stop
      status_change=0;
    }
    else if((modeFlag==4)||(modeFlag==0))
    {
      modeFlag=2;
      FLAG[Auto_blink]=1;
      FLAG[WFlag] =1; 
      FLAG[Buzzer_Mute]=0;
      FLAG[Buzzer_Blink]=0;
    }
  }
  __delay_cycles(1000000);
  __delay_cycles(1000000);
  __delay_cycles(1000000);
  __delay_cycles(1000000);
  P2IFG &= ~BIT3;                         // Clear P2.3 IFG
}

//------------------------------------------------------------------------------
// UART 1
//------------------------------------------------------------------------------

#pragma vector=USCI_A1_VECTOR 
__interrupt void USCI_UART_UCRXISR(void)
{
  if(UCA1RXBUF=='$')
  {
    FLAG[DollarStart]= true; DollarCount=0;
  }
  if(FLAG[DollarStart]== true)
  {
    uart1RxBuff[rxBuffIndex]=UCA1RXBUF;
    rxBuffIndex++;  
  }
  if((UCA1RXBUF=='!')&&(DollarCount<2))
  {
    FLAG[DollarStart]= false; 
    
    if((uart1RxBuff[rxBuffIndex-2])=='W')           //W---> for getting new reading(setting parameters for freash value) of DG
    { 
      rxBuffIndex=0;   
      FLAG[WifiConnect]=true;  
      FLAG[WFlag]=true; 
    }
//------------------------ Decodeing of FRST -----------------------------------  
    else if(((uart1RxBuff[rxBuffIndex-5])=='F')&&((uart1RxBuff[rxBuffIndex-4])=='R')&&((uart1RxBuff[rxBuffIndex-3])=='S')&&((uart1RxBuff[rxBuffIndex-2])=='T')&&(rxBuffIndex>3))
    {
      rxBuffIndex=0;
      DG_Minute=0;
      write_DG(DG_Minute);      
      POWER_Minute=0;
      write_Power(POWER_Minute); 
    }
//------------------------ Decodeing of PS--------------------------------------  
    else if(((uart1RxBuff[rxBuffIndex-2])=='S')&&((uart1RxBuff[rxBuffIndex-3])=='P')&&(rxBuffIndex>1))                    //PS                 
    {   
      FLAG[Pub_OK]=true;
      rxBuffIndex=0;    
    }
//------------------------ Decodeing of E0 -------------------------------------    
    else if(((uart1RxBuff[rxBuffIndex-2])=='0')&&((uart1RxBuff[rxBuffIndex-3])=='E')&&(rxBuffIndex>1))               
    {                                                                           //$E0!----> WiFi Connection is good        
      rxBuffIndex=0;                                                            // 321
      FLAG[WifiBlink]=false;
      e1Counter=0;                       
      FLAG[WifiConnect]=true;
      FLAG[WFlag]=true; 
    }
//------------------------ Decodeing of E1--------------------------------------     
    else if(((uart1RxBuff[rxBuffIndex-2])=='1')&&((uart1RxBuff[rxBuffIndex-3])=='E')&&(rxBuffIndex>1))                    //E1----> Aws Connection is not good
    { 
      FLAG[WifiBlink]=true;
      rxBuffIndex=0; 
      FLAG[WifiConnect]=false; 
      e1Counter=0;
    }
//------------------------ Decodeing of E2--------------------------------------   
    else if(((uart1RxBuff[rxBuffIndex-2])=='2')&&((uart1RxBuff[rxBuffIndex-3])=='E')&&(rxBuffIndex>1))                    
    {                                                                             //$E2!----> WiFi Connection is not good
      rxBuffIndex=0;                                                             // 321
      FLAG[WifiBlink]=false;
      FLAG[WifiConnect]=false;  
      e1Counter=0;
      P1OUT&= ~WiFi_LED;
    }
//------------------------ Decodeing of S1 -------------------------------------   
    else if(((uart1RxBuff[rxBuffIndex-2])=='1')&&((uart1RxBuff[rxBuffIndex-3])=='S')&&(rxBuffIndex>1)&&(modeFlag!=4))                              
    {
      modeFlag=1; 
      P3OUT |= BIT0;
      FLAG[Buzzer_Blink]=0;                                   //S1 mode=on 
      FLAG[WFlag] =1;  
      rxBuffIndex=0;
      P1OUT |= AUTO_LED;
      FLAG[Auto_blink]=0; 
    }
//------------------------ Decodeing of S0 ------------------------------------- 
    else if(((uart1RxBuff[rxBuffIndex-2])=='0')&&((uart1RxBuff[rxBuffIndex-3])=='S')&&(rxBuffIndex>1)&&(modeFlag!=4))                                 
    {
      modeFlag=0;
      P3OUT &= ~BIT0;
      FLAG[Buzzer_Blink]=0;                                  //S0 mode=off   
      FLAG[WFlag] =1;
      rxBuffIndex=0; 
      P1OUT &= ~AUTO_LED;
      FLAG[Auto_blink]=0; 
    }
//------------------------ Decodeing of S2 -------------------------------------   
    else if(((uart1RxBuff[rxBuffIndex-2])=='2')&&((uart1RxBuff[rxBuffIndex-3])=='S')&&(rxBuffIndex>1)&&(modeFlag!=4))                                   
    {
      modeFlag=2; 
      FLAG[Auto_blink]=1;
      FLAG[WFlag] =1; 
      rxBuffIndex=0;      //S2 mode=Auto 
      FLAG[Buzzer_Blink]=0; 
    }  
//------------------------ Decodeing of CON-------------------------------------  
    else if(((uart1RxBuff[rxBuffIndex-4])=='C')&&((uart1RxBuff[rxBuffIndex-3])=='O')&&((uart1RxBuff[rxBuffIndex-2])=='N')&&(rxBuffIndex>3))
    {                                                                           //CON!
      FLAG[Fram_WR] =true;                                                      //4321
      rxBuffIndex=0;
      licActive=0;
      FLAG[WifiConnect]=true;                 
    }
//------------------------ Decodeing of COFF-------------------------------------   
    else if(((uart1RxBuff[rxBuffIndex-5])=='C')&&((uart1RxBuff[rxBuffIndex-4])=='O')&&((uart1RxBuff[rxBuffIndex-3])=='F')&&((uart1RxBuff[rxBuffIndex-2])=='F')&&(rxBuffIndex>3))
    {
      FLAG[Fram_WR] =true;                                                      //COFF!
      rxBuffIndex=0;                                                            //54321
      licActive=1; 
      FLAG[WifiConnect]=true;                 
    }
 }
 if(rxBuffIndex>55)
 {
   rxBuffIndex=0;  
 }
}