--------------------------------------------------------------------------------------
                  Inverted Sump control (Kanakadhara Project) 
--------------------------------------------------------------------------------------
 

Settings Order:
In order to enter into Settings,Press the switch and Hold it before
switch ON the device.
---------------------------------------------
Motor Type: Starter
---------------------------------------------
|   Parameter   |           Range           |
---------------------------------------------
|               |                           |   
| Minute        |  0-59  min (0,1,2,.....59)|
|               |                           |
| Hour          |  0-23  hrs (0,1,2,.....23)|
|               |                           |
| Dryrun Time   |  2-10  min (0,2,.......10)|
|               |                           |
| Start Level   |  75-95 %   (75,80,.....95)|
|               |                           |
| Stop Level    |  5-40 %    (5,10,.....40) |
|               |                           |
---------------------------------------------



Note: This device is used to to empty the tank and dryrun beep
will be generated when there is no decrease in level.