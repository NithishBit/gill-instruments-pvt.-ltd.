------------------------------------------------------------------------
|                            S1 Model                                  |
------------------------------------------------------------------------
Settings Order:
     In order to enter into Settings,Press the switch and Hold it before
     switch ON the device.

---------------------------------------------
|   Parameter   |           Range           |
---------------------------------------------
|               |                           |   
| Minute        |  0-59  min (0,1,2,.....59)|
|               |                           |
| Hour          |  0-23  hrs (0,1,2,.....23)|
|               |                           |
| Dryrun Time   |  0-10  min (0,2,.......10)|
|               |                           |
| Start Level   |  75-95 %   (75,80,.....95)|
|               |                           |
| Stop Level    |  5-40 %    (5,10,.....40) |
|               |                           |
| Tank Mode     | Single tank(Tank A LED ON |
|               | Dual Tank  (Tank B LED ON |   
---------------------------------------------
Once the setting entered into Tank Mode Tank A and B LED will toggle untill we select
the mode. If we select Dual Tank it will enter into sump control Level (Range: 5,10,..40)
and if it is single Tank normal boot will happen.

1) Dryrun can be disabled by selecting "0" as dryrun time.

2) Inter lock mechanism is included in this model.