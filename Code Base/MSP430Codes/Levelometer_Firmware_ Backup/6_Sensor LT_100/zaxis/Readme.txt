;----------------------------------------------------------------------------------------;
;Program for level sensor v1.4       
;used with level-o-meter base unit
;dated 21 AUG 2012
;IAR Systems Embedded Workbench 6.4 Kickstart.

;Manoj Kumar.S
;Gill Instruments.
;----------------------------------------------------------------------------------------;
;version 1.4(stable)
;PRODUCTION READY
;----------------------------------------------------------------------------------------;
;----------------------------------------------------------------------------------------;
;CHANGES
;----------------------------------------------------------------------------------------;
;Lookup table is created based on the Z-axis(sensor is in horizontal position).
;For lower side search in lookup table Pointer(R15)is initialy pointed at Oth position(3Fh)
;For upper side search in lookup table Pointer(R15)is initialy pointed at 51th Position(FEh)..
;Noise reduction is done based on the (R15)Offset value.
;Dummy values added in lookup table for calibration(to make the output 99%)[if needed]
;----------------------------------------------------------------------------------------;