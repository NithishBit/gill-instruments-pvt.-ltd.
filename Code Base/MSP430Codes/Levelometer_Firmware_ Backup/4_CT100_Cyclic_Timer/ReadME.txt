------------------------------------------------------------------
------------------------------------------------------------------
                          Super Timer
------------------------------------------------------------------
------------------------------------------------------------------

Settings Order:
In order to enter into Settings,Press the switch and Hold it before
switch ON the device.
---------------------------------------------
Motor Type: Starter
---------------------------------------------
|   Parameter   |           Range           |
---------------------------------------------
|               |                           |   
| Minute        |  0-59  min (0,1,2,.....59)|
|               |                           |
| Hour          |  0-23  hrs (0,1,2,.....23)|
|               |                           |
| ONtime        |  10-90 min (10,20,30,..90)|
|               |                           |
| OFFtime       |  1-20  hrs (1,2,3,.....20)|
|               |                           |
| Stop Level    |  75-95 %   (75,80,.....95)|
|               |                           |
| Dryrun Time   |  2-10  min (2,4,.......19)|
---------------------------------------------


    1)  During OFF time TANK A LED is ON and During OFF time TANK B LED is ON.
        If you try to Switch ON the Motor during OFF time the Fault LED will
        Blink. During ON time manual ON/OFF is Possible.

    2)  Start Level Threshold is automatically calculated from
        Stop level as follow (Start Level= Stop Level-10)ex: 90-10=80 

    3)  if the water level is below the Lower level threshold automatic ON/OFF cyclic will happen.
        Above the bottom level threshold the device will not switch ON the motor automatically during ON time
        but still LED will be Indicating that off time is over and LED indication switched to ONtime period.

    4)  During this period manual ON is possible otherwise the Motor will go ON once the Level reduced below the bottom level.
