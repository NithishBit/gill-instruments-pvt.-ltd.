//******************************************************************************
// This program read Temprature/Humidity from SHT3x using HW I2c Protocol
// ESP32 HAS DUAL GATEWAY ID
//
//                MSP430FR2633
//             -----------------                         SHT3X
//         /|\|             P1.0|-->LED                |--------|
//          | |             P1.1|                      |        |
//          | |         P1.2/SDA|--------------------->|        |
//          | |         P1.3/SCL|<---------------------|        |
//          --|RST          P1.6|-->LED/D2 (TX)        |        |
//            |             P1.7|-->LED/D3 (cloud)     |--------|
//            |             P2.3|<--SW
//            |             P2.4|-->SW_LED 
//            |             P3.0|-->Relay 
//            |             P3.1|-->EN/ESP32  
//            |       (RXD1)P2.5|<--TXD0/ESP32
//            |       (TXD1)P2.6|-->RXD0/ESP32 
//            |_________________|
//
//  Gurjit/Kiran/Kishore
//  Gill Instruments.
//  05 May 2019
//  Built with IAR Embedded Workbench v6.50
//******************************************************************************
#include <msp430fr2633.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include "TH_files.h"
#include "TH_Read.h"
//------------------------------------------------------------------------------
// master switch Mode selection
// modeFlag=0--> OFF
// modeFlag=1--> ON
// modeFlag=2--> AUTO
// modeFlag=4--> Emergency stop

//******************************************************************************

main(void)
{
  WDTCTL = WDT_ADLY_1000;                                                       // 1000ms
  SFRIE1 |= WDTIE;                                                              // Enable WDT interrupt
  Init_GPIO();
  DcoConfig();                   
  UartOneInit();
  Flash_ptrACTIVE= (unsigned long *)FRAM_ACTIVE;
  licActive=*Flash_ptrACTIVE;
//------------------------------------------------------------------------------
  power_up(); 
  ResetWifi();
  FLAG[Auto_blink]=2;
  P2IFG &= ~BIT3;                                                               // P2.3 IFG cleared
  
   init_I2C();                                                                  // Configure USCI_B0 for I2C mode
  
   writeCMD(0x2B,0x32);                                                         // sending command 0x2B32 to sensor (periodic measurement) 
//------------------------------------------------------------------------------  
  while (1)                                 
  {
      if(FLAG[Fram_WR]==1)
    {
      writeActive(licActive);                                                   // write license 
      FLAG[Fram_WR]=0;
    }  
//-------------------------------------------------------------------------------    
    if((FLAG[RstFlag]==true)||(errCounter>120))                                 // condition for reset
    {
      ResetWifi(); 
      FLAG[RstFlag]=0;
      errCounter=0;
    }
//------------------------------------------------------------------------------- 
    if((wd_count >2)||(FLAG[WFlag]==1))                                         // (Transmit temp/Humidity @ 2 sec interval)
    {
      getTH();                                                                  // read temp, Humidity from sensor
   
      TxCloudNotify();                                                          // status,Temparture ,Humidity,modeFlag 
      wd_count=0;FLAG[WFlag]=0;
    }
 
      __bis_SR_register(LPM3_bits|GIE);                                         // Enter LPM3 w/interrupt
      __no_operation();  
  }  
}

//*************************** END OF MAIN **************************************


//------------------------------------------------------------------------------
// Watchdog Timer interrupt service routine
//------------------------------------------------------------------------------
 #pragma vector=WDT_VECTOR
__interrupt void watchdog_timer(void)

{
  wd_count++;  wdt_time++;                                                      // wi-fi  count  

//--------------------------------Wi-Fi status ---------------------------------
  if(FLAG[WifiConnect]==true)
  {
    errCounter =0;
    PS_Count++; 
    P1OUT|= WiFi_LED;   
  }
  else
  {
    errCounter++;
  }
  
  if(FLAG[Auto_blink]==1)
  { 
    P1OUT ^= AUTO_LED;
  }
  if(FLAG[WifiBlink]==1)
  {
    P1OUT ^= WiFi_LED;
  }
//---------------------------Publish ACK ---------------------------------------
  if((PS_Count==10)&&(FLAG[Pub_OK]!=1))
  {
    FLAG[WFlag] =1;  PS_Count=0; psCount++;
  }
  if(psCount>3)
  {
    FLAG[RstFlag]=1; 
    psCount=0;
    FLAG[WifiConnect]=0;
  }
   if((wdt_time>9))
  {
    wdt_time=0; 
    //BigFlag[SocomecTxPara]=0;
    // __bic_SR_register_on_exit(LPM3_bits);
  }
    if(wd_count >850)
  {
    wd_count=0;
  }
  if(FLAG[Pub_OK]==1)                                                           // In previous session if PUB ok then reset counts
  {
    psCount=0;
    PS_Count=0;
  }
  __bic_SR_register_on_exit(LPM3_bits);                                         // Clear LPM3 bits from 0(SR)
}
//======================End of WDT ISR =========================================

//------------------------------------------------------------------------------
// Port 2 interrupt service routine // Master Switch
//------------------------------------------------------------------------------
 #pragma vector=PORT2_VECTOR
__interrupt void Port_2(void)
 {
  if((FLAG[Buzzer_Blink]==1)&&(Buzzer_Counter>9))
  {
    P3OUT &= ~BIT2;           // Mute Logic
    Buzzer_Counter =0;
    P3OUT &= ~BIT0;
    FLAG[Buzzer_Mute]=1;
  }
  else
  {
    if(modeFlag==2)
    {
      modeFlag=1;             // This code is exc for ON mode 
      P3OUT |= BIT0;  
      P1OUT |= AUTO_LED;
      FLAG[Auto_blink]=0;
      FLAG[Buzzer_Mute]=0;
      Buzzer_Counter =0;
      Notify=4;               //Emergency Start
    //  status_change=0;
    }
    else if(modeFlag==1)
    {
      modeFlag=4;             // This code is exc for OFF mode 
      P3OUT &= ~BIT0;
      P1OUT &= ~AUTO_LED;
      FLAG[Auto_blink]=0;
      FLAG[Buzzer_Mute]=0;
      Buzzer_Counter =0;
      Notify=3;               //Emergency Stop
    //  status_change=0;
    }
    else if((modeFlag==4)||(modeFlag==0))
    {
      modeFlag=2;
      FLAG[Auto_blink]=1;
      FLAG[WFlag] =1; 
      FLAG[Buzzer_Mute]=0;
      FLAG[Buzzer_Blink]=0;
    }
  }
  __delay_cycles(1000000);
  __delay_cycles(1000000);
  __delay_cycles(1000000);
  __delay_cycles(1000000);
  P2IFG &= ~BIT3;             // Clear P2.3 IFG
}

//------------------------------------------------------------------------------
// UART 1 (ESP32)
//------------------------------------------------------------------------------

#pragma vector=USCI_A1_VECTOR 
__interrupt void USCI_UART_UCRXISR(void)
{
  if(UCA1RXBUF=='$')
  {
    FLAG[DollarStart]= true; DollarCount=0;
  }
  if(FLAG[DollarStart]== true)
  {
    uart1RxBuff[rxBuffIndex]=UCA1RXBUF;
    rxBuffIndex++;  
  }
  if((UCA1RXBUF=='!')&&(DollarCount<2))
  {
    FLAG[DollarStart]= false; 
//------------------------ Decodeing of $W! ------------------------------------     
    if((uart1RxBuff[rxBuffIndex-2])=='W')  //W---> Request for Data from cloud
    { 
      rxBuffIndex=0;   
      FLAG[WifiConnect]=true;  
      FLAG[WFlag]=true; 
    }

//------------------------ Decodeing of $PS! -----------------------------------  
    else if(((uart1RxBuff[rxBuffIndex-2])=='S')&&((uart1RxBuff[rxBuffIndex-3])=='P')&&(rxBuffIndex>1))                    //PS                 
    {   
      FLAG[Pub_OK]=true;
      rxBuffIndex=0;    
    }
//------------------------ Decodeing of $E0! -----------------------------------    
    else if(((uart1RxBuff[rxBuffIndex-2])=='0')&&((uart1RxBuff[rxBuffIndex-3])=='E')&&(rxBuffIndex>1))               
    {                                                                           //$E0!----> WiFi Connection is good        
      rxBuffIndex=0;                                                            // 321
      FLAG[WifiBlink]=false;
      errCounter=0;                       
      FLAG[WifiConnect]=true;
      FLAG[WFlag]=true; 
    }
//------------------------ Decodeing of $E1! -----------------------------------     
    else if(((uart1RxBuff[rxBuffIndex-2])=='1')&&((uart1RxBuff[rxBuffIndex-3])=='E')&&(rxBuffIndex>1))                    //E1----> Aws Connection is not good
    { 
      FLAG[WifiBlink]=true;
      rxBuffIndex=0; 
      FLAG[WifiConnect]=false; 
      errCounter=0;
    }
//------------------------ Decodeing of $E2!------------------------------------   
    else if(((uart1RxBuff[rxBuffIndex-2])=='2')&&((uart1RxBuff[rxBuffIndex-3])=='E')&&(rxBuffIndex>1))                    
    {                                                                             //$E2!----> WiFi Connection is not good
      rxBuffIndex=0;                                                             // 321
      FLAG[WifiBlink]=false;
      FLAG[WifiConnect]=false;  
      errCounter=0;
      P1OUT&= ~WiFi_LED;
    }
//------------------------ Decodeing of $S1! (RLY ON) --------------------------   
    else if(((uart1RxBuff[rxBuffIndex-2])=='1')&&((uart1RxBuff[rxBuffIndex-3])=='S')&&(rxBuffIndex>1)&&(modeFlag!=4))                              
    {
      modeFlag=1; 
      P3OUT |= BIT0;
      FLAG[Buzzer_Blink]=0;                                   //S1 mode=on 
      FLAG[WFlag] =1;  
      rxBuffIndex=0;
      P1OUT |= AUTO_LED;
      FLAG[Auto_blink]=0; 
    }
//------------------------ Decodeing of $S0! (RLY OFF)-------------------------- 
    else if(((uart1RxBuff[rxBuffIndex-2])=='0')&&((uart1RxBuff[rxBuffIndex-3])=='S')&&(rxBuffIndex>1)&&(modeFlag!=4))                                 
    {
      modeFlag=0;
      P3OUT &= ~BIT0;
      FLAG[Buzzer_Blink]=0;                                  //S0 mode=off   
      FLAG[WFlag] =1;
      rxBuffIndex=0; 
      P1OUT &= ~AUTO_LED;
      FLAG[Auto_blink]=0; 
    }
//------------------------ Decodeing of $S2! (RLY AUTO) ------------------------   
    else if(((uart1RxBuff[rxBuffIndex-2])=='2')&&((uart1RxBuff[rxBuffIndex-3])=='S')&&(rxBuffIndex>1)&&(modeFlag!=4))                                   
    {
      modeFlag=2; 
      FLAG[Auto_blink]=1;
      FLAG[WFlag] =1; 
      rxBuffIndex=0;      //S2 mode=Auto 
      FLAG[Buzzer_Blink]=0; 
    }  
//------------------------ Decodeing of $CON!(PUB ENABLE)-----------------------  
    else if(((uart1RxBuff[rxBuffIndex-4])=='C')&&((uart1RxBuff[rxBuffIndex-3])=='O')&&((uart1RxBuff[rxBuffIndex-2])=='N')&&(rxBuffIndex>3))
    {                                                                           //CON!
      FLAG[Fram_WR] =true;                                                      //4321
      rxBuffIndex=0;
      licActive=0;
      FLAG[WifiConnect]=true;                 
    }
//------------------------ Decodeing of $COFF!(PUB DISABLE)---------------------   
    else if(((uart1RxBuff[rxBuffIndex-5])=='C')&&((uart1RxBuff[rxBuffIndex-4])=='O')&&((uart1RxBuff[rxBuffIndex-3])=='F')&&((uart1RxBuff[rxBuffIndex-2])=='F')&&(rxBuffIndex>3))
    {
      FLAG[Fram_WR] =true;                                                      //COFF!
      rxBuffIndex=0;                                                            //54321
      licActive=1; 
      FLAG[WifiConnect]=true;                 
    }
 }
 if(rxBuffIndex>55)
 {
   rxBuffIndex=0;  
 }
}
//------------------------------------------------------------------------------               