//******************************************************************************
//Energy meter---RS485---modbus--protocol for Socomec meter
//2 Word reading of socomec three phase energy Meter (COUNTIS -E03)
//Restart ESP32 
//                MSP430FR2633
//             -----------------
//         /|\|             P1.0|-->LED/D5(Relay)
//          | |             P1.1|<-- TA0.CCI1A/CAP1
//          | |     P1.4/UCA0TXD|----> [RS485](A)---->Socomec e03
//          | |     P1.5/UCA0RXD|<---- [RS485](B)---->Socomec
//          --|RST          P1.6|-->LED/D2 (TX)  
//            |             P1.7|-->LED/D3 (cloud) 
//            |             P2.3|<--SW
//            |             P2.4|-->SW_LED 
//            |             P3.0|-->Relay 
//            |             P3.1|-->EN/ESP32  
//            |       (RXD1)P2.5|<--TXD0/ESP32
//            |       (TXD1)P2.6|-->RXD0/ESP32 
//            |_________________|
//  Pradhan V
//  Gill Instruments.
//  21 Aug 2018
//  Built with IAR Embedded Workbench v6.50
//******************************************************************************
#include <msp430fr2632.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <math.h>
#include "myfile.h"

                //slave_id 28,read reg 03,Energy reg 0xC702,Reg length 0x02//
char Energy[8] = {0X10,0X03,0X010,0X48,0X00,0X02};                 //Total Positive Active Energy (UNIT:kWh/100)
char Power_factor[8] ={0X10,0X03,0X010,0X34,0X00,0X02};            //Total Positive Active Energy (UNIT:kWh)
char Energy_kwh[8] ={0X10,0X03,0X010,0X3E,0X00,0X02};              //Active Power phase 1 +/- : P1(UNIT: kW/100(Signed))
char Energy_buf[40],Volt_buf[30],Current_buf[25],PF_buf[25],KWH_buf[25],uart1RxBuff[60];
char slave=0;
long long int digi[5];
volatile unsigned int e1Counter=0,psWdtCount=0,psCount=0,crc_out=0,DollarCount=0,SocomecCount=0;
unsigned int rs485ReadFailCount=0,resetCount=0,rxBuffIndex=0;
unsigned int RX_index =0,inter=0,TX_index = 0,wd_count=0,wdt_time=0;
volatile unsigned long *FRAM_write_ptr;
volatile unsigned long *Flash_ptrACTIVE;   // Harware Lic Active
volatile float *Flash_ptrPF;               // PF Value
volatile unsigned long *Flash_ptrHIGH;     // High Alert Value 
volatile unsigned long data;
unsigned long licActive=0,highAlertRx=0,highAlert=0;
int matchCount = 0;float PF=0,PfRx=0;
bool FLAG[15] = {0};
long double Energy_MSB=0;uint32_t num;
long double energyKW=0,FreshEnergyKW=0,PowerFactor=0;
volatile short int BigFlag[5]= {0};
volatile unsigned int Counter[10] = {0};
//==============================================================================
void main(void)
{
  WDTCTL = WDT_ADLY_1000;                   // WDT 32ms, SMCLK, interval timer
  Init_GPIO();                               // Configure GPIO
  Board_Init();
  PM5CTL0 &= ~LOCKLPM5;                      // Disable the GPIO power-on default high-impedance mode
  power_up();                               
  ResetWifi();
   
  FRAM_write_ptr = (unsigned long *)FRAM_TEST_START;
  Flash_ptrHIGH= (unsigned long *)FRAM_HIGH_ALERT;
  Flash_ptrPF = (float *)FRAM_PF_ALERT; 
  Flash_ptrACTIVE= (unsigned long *)FRAM_ACTIVE;
  
  SFRIE1 |= WDTIE; 
//------------------------------------------------------------------------------
  slave=*FRAM_write_ptr ;
  licActive=*Flash_ptrACTIVE;
  highAlertRx=highAlert=*Flash_ptrHIGH;  
  PF=*Flash_ptrPF;
  P1OUT =BIT2; P2IFG &= ~BIT3;                         // Clear P2.3 IFG
  while(1)
  {
    if((BigFlag[SocomecTxPara]<3))
    {
   __delay_cycles(900000);
    socomec_transmit_rs485();
    __bis_SR_register(LPM0_bits|GIE);     // Enter LPM0
    __no_operation();
  // __delay_cycles(900000);
    } 
    else{
    if((FLAG[WFlag]==1))
    {
      FLAG[WFlag]=0;wd_count=0;            //Buzzer();
      PublishEnergy();
     }
    if(FLAG[FramActiveFlag]==1)
    {
     writeActive(licActive);  
     FLAG[FramActiveFlag]=0;
    }
    if( FLAG[FramSlaveIDFlag] ==1)
    {   FLAG[FramSlaveIDFlag]=0;
     FramSlaveIDWrite();
    }
    if((highAlertRx!=highAlert)||(highAlertRx<99))
    {
      highAlert=highAlertRx;
      writeAlert(highAlert);   
    }
    if((FLAG[RstFlag]==1)||((e1Counter>45)&&(e1Counter<65)))
    {
     ResetWifi(); 
     FLAG[RstFlag]=0;
    }
    if((PfRx!=PF)&&(PfRx<1.0))
    {PF = PfRx;
     writePf (PF); 
    }
    __bis_SR_register(LPM0_bits|GIE);     // Enter LPM0
    __no_operation();
   }
  }
  //return 0;
}
//______________________________________________________________________________
void ResetWifi(void)
{ 
  Delay(); 
  P3OUT |= BIT1;
  Delay(); 
  P3OUT &= ~BIT1;
  Delay(); 
  P3OUT |= BIT1;
  P1OUT&=~BIT7;
}
//______________________________________________________________________________
void conversion(void)
{ int conversionFlag=0; 
  switch((BigFlag[SocomecTxPara]))
  {
  case 1:if(Counter[socomecParameter] == 1)
  {
  BuffConversion(RX_index-3);
  if(Energy_MSB!=*((float*)&digi[4]))
  conversionFlag =1;
  Energy_MSB=*((float*)&digi[4]);
  }
  else
  {
     wdt_time=0; SocomecCount=0;
    BigFlag[SocomecTxPara]=0;matchCount = 0; 
  }
  break;
  
  case 2:if(Counter[socomecParameter] == 2)
  {
   BuffConversion(RX_index-3);
   num = (long int)digi[4];
   FreshEnergyKW=(*((float*)&num))/1000;
  if(((FreshEnergyKW -0.50)>energyKW)||((FreshEnergyKW +0.50)<energyKW))
  conversionFlag =1;
  energyKW=FreshEnergyKW;
 if((energyKW>highAlert)&&(BigFlag[NtfyFlag]!=1))
  { P3OUT |= BIT0;
   BigFlag[NtfyFlag] = 1;  Counter[buzzer]=7;
  }else if((energyKW<highAlert)&&(BigFlag[NtfyFlag]==1))  BigFlag[NtfyFlag] = 0;
   }
  else
  {
    wdt_time=0; SocomecCount=0;
    BigFlag[SocomecTxPara]=0;matchCount = 0;
  }
   break;
   
  case 3:if(Counter[socomecParameter] == 3)
  {
    BuffConversion(RX_index-3);
    PowerFactor=*((float*)&digi[4]);
    if((PowerFactor<PF)&&(BigFlag[NtfyFlag] != 3))
    { BigFlag[NtfyFlag] = 3; Counter[buzzer]=3;}
    if( BigFlag[NtfyFlag] ==2) BigFlag[NtfyFlag] = 0;
    FLAG[OneTimeRead]=1;FLAG[Rs485RcvFlag]=0;FLAG[SocoReadFail]=0;
    }
  else
  {
     wdt_time=0; SocomecCount=0;
    BigFlag[SocomecTxPara]=0;matchCount = 0;
  }
    break;
    
  default:
    break;
  }
  if(conversionFlag==1){ FLAG[WFlag] =1;conversionFlag=0;}
 }
//______________________________________________________________________________
void PublishEnergy(void)
{
  if((FLAG[WifiConnect]==1)&&(FLAG[OneTimeRead]==1))
{
  P1OUT |= BIT6; FLAG[PsWdtFlag]=0;
  snprintf(Energy_buf,35,"$%5.3Lf;%.2Lf;%d;0;%ld;%.2Lf;!",energyKW,Energy_MSB,BigFlag[NtfyFlag],highAlert,PowerFactor);        //$XX;Y;Z;@!  modeFlag ntfy_flag
  if(licActive!=1)Tx_Uart(Energy_buf);
  P1OUT &= ~BIT6; psWdtCount=0;                 
   }
  else if(FLAG[WifiConnect]==1)
  {FLAG[PsWdtFlag]=0;
   snprintf(Energy_buf,15,"$ ; ;%d;0;%ld;!",BigFlag[NtfyFlag],highAlert);        //XX;Y;Z;@  modeFlag ntfy_flag
  if(licActive!=1)Tx_Uart(Energy_buf); 
  psWdtCount=0;         
  }
}
//______________________________________________________________________________
void BuffConversion(long long int n)
{ 
  digi[0]=((((long long int)RX_BUFFER[n-2]))*16777216);     //16^6=16777216
  digi[1]=((((long long int)RX_BUFFER[n-1]))*65536);        //16^4=65536
  digi[2]=(((long long int)RX_BUFFER[n]))*256;              //16^2=256
  digi[3]=((long long int)RX_BUFFER[n+1]);       //16^0=1 //
  digi[4]=(long long int)(digi[0]+digi[1]+digi[2]+digi[3]);
  //z = *((float*)&digi[4]);
  //z = z+ digi[4];
}
//-------------------------FRAM-------------------------------------------------
void FramSlaveIDWrite (void)
{ //BigFlag[SocomecTxPara]=0;
  SYSCFG0 = FRWPPW | PFWP;
   *FRAM_write_ptr=data;
  SYSCFG0 = FRWPPW | PFWP | DFWP;
}
void writeActive (unsigned long licActive)
{
  SYSCFG0 = FRWPPW | PFWP;
  *Flash_ptrACTIVE= licActive;                   // Write value to flash
  SYSCFG0 = FRWPPW | PFWP | DFWP;
 }
void writeAlert (unsigned long highAlert)
{
  SYSCFG0 = FRWPPW | PFWP;
  *Flash_ptrHIGH = highAlert;                   // Write value to flash
  SYSCFG0 = FRWPPW | PFWP | DFWP;
}
void writePf (float PF)
{
  SYSCFG0 = FRWPPW | PFWP;
  *Flash_ptrPF = PF;                   // Write value to flash
  SYSCFG0 = FRWPPW | PFWP | DFWP;
}
//------------------------------------------------------------------------------
void socomec_transmit_rs485()
{
  Energy[0]=Power_factor[0]=Energy_kwh[0]=slave; //Volt[0]=Current[0]=
  Delay();
  switch(BigFlag[SocomecTxPara])
  {
  case 0:
  TX_index=0;RX_index=0;P2OUT|= BIT2;  BigFlag[SocomecTxPara]=1;inter=7;         //P2.0 -> DE (high) transmit
  P1OUT |=BIT0; Counter[socomecParameter] = 0;  FLAG[Rs485RcvFlag]=1;//rs485ReadFailCount =0;
  crc_out=crc16(Energy,6);
  Energy[6]=crc_out;
  Energy[7]=(crc_out/256);
  while(TX_index < 8)
  {
    UCA0TXBUF = Energy[TX_index++];     //"slave id,read register,register address,number of words to read,CRC"}
     while(!(UCA0IFG & UCTXIFG));
  }
  __delay_cycles(7000);P2OUT&= ~BIT2;//P2.0 -> DE (low)
  break;

//----------------- End of case:0-----------------------------------------------
 
  case 1:
  TX_index=0;RX_index=0;P2OUT|= BIT2; BigFlag[SocomecTxPara]=2;inter=14;//P2.0 -> DE (high) transmit
  matchCount =7;
  crc_out=crc16(Power_factor,6);
  Power_factor[6]=crc_out;
  Power_factor[7]=(crc_out/256);
  while(TX_index < 8)
  {while(!(UCA0IFG & UCTXIFG));
    UCA0TXBUF = Power_factor[TX_index++];
    while(!(UCA0IFG & UCTXIFG));
  }
  __delay_cycles(7000);P2OUT&= ~BIT2;//P2.0 -> DE (low)
  break;
//----------------- End of case:3-----------------------------------------------
  case 2:
  TX_index=0;RX_index=0;P2OUT|= BIT2; BigFlag[SocomecTxPara]=3;inter=21; //P2.0 -> DE (high) transmit
  matchCount = 14;
  crc_out=crc16(Energy_kwh,6);
  Energy_kwh[6]=crc_out;
  Energy_kwh[7]=(crc_out/256);

  while(TX_index < 8)
  {
    UCA0TXBUF = Energy_kwh[TX_index++];
    while(!(UCA0IFG & UCTXIFG));
  }
  __delay_cycles(7000);P2OUT&= ~BIT2; P1OUT &=~ BIT0;//P2.0 -> DE (low)
  break;
//----------------- End of case:4-----------------------------------------------
  default: break;
  }
}

//______________________________________________________________________________
#pragma vector =USCI_A0_VECTOR //USCI_A0_VECTOR (RS-485 Recive Int)
__interrupt void USCI_A0_ISR(void)
{
  //__bic_SR_register(GIE);   
  switch(__even_in_range(UCA0IV,USCI_UART_UCTXCPTIFG))
  {
  case USCI_NONE: break;
  case USCI_UART_UCRXIFG:
   
    RX_BUFFER[RX_index]=UCA0RXBUF;    //index overflow
    RX_index++;
    if((UCA0RXBUF==03)&&(RX_BUFFER[RX_index-2]==slave))
      FLAG[SocoSlaveMatch]=true;
   if((FLAG[Rs485RcvFlag]==1)&&(FLAG[SocoSlaveMatch]==true))
     matchCount++;

    if(RX_index>30)
    {
      RX_index=0; 
    }
    if((inter==matchCount)&&(inter!=0))
    {    Counter[socomecParameter]++;
    rs485ReadFailCount=0;FLAG[SocoSlaveMatch]= false;matchCount++;
     conversion();  
      __bic_SR_register_on_exit(LPM0_bits);
    }
    break;
  case USCI_UART_UCTXIFG: break;
  case USCI_UART_UCSTTIFG: break;
  case USCI_UART_UCTXCPTIFG: break;
  }
    //__bis_SR_register(GIE);
}
//______________________________________________________________________________
#pragma vector=USCI_A1_VECTOR  //(ESP32 Recive Int)
__interrupt void USCI_UART_UCRXISR(void)
{
  if(UCA1RXBUF=='$')
  {
  FLAG[DollarStart]= true; DollarCount=0;
  }
  if(FLAG[DollarStart]== true)
  {
  uart1RxBuff[rxBuffIndex]=UCA1RXBUF;
  rxBuffIndex++;  
  }
  if((UCA1RXBUF=='!')&&(DollarCount<3))
  {
  FLAG[DollarStart]= false; 

  if((uart1RxBuff[rxBuffIndex-2])=='W')           //W---> for getting new reading(setting parameters for freash value) of energy meter
   { rxBuffIndex=0;    
     FLAG[WFlag]=1; 
     __bic_SR_register_on_exit(LPM0_bits); 
      }
  else if(((uart1RxBuff[rxBuffIndex-2])=='#')&&((uart1RxBuff[rxBuffIndex-3])=='^')&&(rxBuffIndex>1))      //$28^#!----> for setting new slave id
   {   
     FLAG[FramSlaveIDFlag]=1;                                                                     // 54321
     slave=uart1RxBuff[rxBuffIndex-5];
     slave=((slave-'0')*10)+((uart1RxBuff[rxBuffIndex-4])-'0');
     data = slave;
     rxBuffIndex=0;BigFlag[SocomecTxPara]=5;
     __bic_SR_register_on_exit(LPM0_bits); 
   }
  else if(((uart1RxBuff[rxBuffIndex-2])=='0')&&((uart1RxBuff[rxBuffIndex-3])=='E')&&(rxBuffIndex>1))               
  {                                                                           //$E0!----> WiFi Connection is good        
    P1OUT|= BIT7; rxBuffIndex=0;  FLAG[WifiBlink]=0; e1Counter=70;            // 321
    FLAG[WifiConnect]=1;FLAG[WFlag]=1; resetCount=0;                                           
  }
  else if(((uart1RxBuff[rxBuffIndex-2])=='2')&&((uart1RxBuff[rxBuffIndex-3])=='E')&&(rxBuffIndex>1))                    
  {                                                                                   //$E2!----> WiFi Connection is not good
    P1OUT&=~BIT7; rxBuffIndex=0; FLAG[WifiBlink]=0;e1Counter=0; FLAG[WifiConnect]=0;  // 321
  }
    else if(((uart1RxBuff[rxBuffIndex-2])=='1')&&((uart1RxBuff[rxBuffIndex-3])=='E')&&(rxBuffIndex>1))                    //E1----> Aws Connection is not good
    { 
      FLAG[WifiBlink]=1; rxBuffIndex=0; FLAG[WifiConnect]=0; e1Counter=0;
    }
   else if(((uart1RxBuff[rxBuffIndex-2])=='S')&&((uart1RxBuff[rxBuffIndex-3])=='P')&&(rxBuffIndex>1))                    //PS                 
    {   
     FLAG[PsWdtFlag]=1; rxBuffIndex=0;   psCount=0; 
    }
    else if(((uart1RxBuff[rxBuffIndex-2])=='T')&&((uart1RxBuff[rxBuffIndex-3])=='S')&&((uart1RxBuff[rxBuffIndex-4])=='R')&&(rxBuffIndex>2))
    {                                                                           
     FLAG[RstFlag]=1; rxBuffIndex=0;                               
    }
    else if(((uart1RxBuff[rxBuffIndex-2])=='N')&&((uart1RxBuff[rxBuffIndex-3])=='O')&&((uart1RxBuff[rxBuffIndex-4])=='C')&&(rxBuffIndex>2))
    {                                                                           //CON
      FLAG[FramActiveFlag] =1;  rxBuffIndex=0; licActive=0;                                //321
    }
    else if(((uart1RxBuff[rxBuffIndex-2])=='F')&&((uart1RxBuff[rxBuffIndex-3])=='F')&&((uart1RxBuff[rxBuffIndex-4])=='O')&&((uart1RxBuff[rxBuffIndex-4])=='C')&&(rxBuffIndex>3))
    {
      FLAG[FramActiveFlag] =1;  rxBuffIndex=0; licActive=1;                                //COFF
    }                                                                            //4321
    else if(((uart1RxBuff[rxBuffIndex-7])=='H')&&(rxBuffIndex>5))           
    {  
      char levelAlertBuff=0;                                                         //$H002.0!
    levelAlertBuff=((uart1RxBuff[rxBuffIndex-5])-'0');                          //3210987654321
    levelAlertBuff=((levelAlertBuff)*10)+((uart1RxBuff[rxBuffIndex-4])-'0');   
    highAlertRx=levelAlertBuff;                                                  
    BigFlag[SocomecTxPara]=5;  // for not entering socomec loop
    __bic_SR_register_on_exit(LPM0_bits); 
    }
    else if(((uart1RxBuff[rxBuffIndex-6])=='P')&&((uart1RxBuff[rxBuffIndex-5])=='F')&&(rxBuffIndex>5))           
    {  
    char PFCalculate=0;                                                            //$PF1.0!
    PFCalculate=((uart1RxBuff[rxBuffIndex-4])-'0');                          //3210987654321
    PFCalculate=((PFCalculate)*10)+((uart1RxBuff[rxBuffIndex-2])-'0');    
    PfRx=PFCalculate;   
    PfRx = (10*PfRx)/100;
    BigFlag[SocomecTxPara]=5;  // for not entering socomec loop
    __bic_SR_register_on_exit(LPM0_bits); 
    }
  
if(rxBuffIndex>55)
 {
  rxBuffIndex=0;  
 }
 }  
}
//------------------------------------------------------------------------------
// Port 2 interrupt service routine (P2.3)
#pragma vector=PORT2_VECTOR
__interrupt void Port_2(void)
{
  Counter[buzzer]=0;BigFlag[NtfyFlag]=0;
  P3OUT &= ~BIT0;                         // Relay P3.0 OFF
  P3OUT &= ~BIT2;                         // Buzzer P3.2 OFF
  P2IFG &= ~BIT3;                         // Clear P2.3 IFG
}
//______________________________________________________________________________
// Watchdog Timer interrupt service routine
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=WDT_VECTOR
__interrupt void WDT_ISR(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(WDT_VECTOR))) WDT_ISR (void)
#else
#error Compiler not supported!
#endif
{ 
  wdt_time++;  wd_count++;//e1Counter++;
  if(Counter[buzzer]>1)
  {
    P3OUT ^= BIT2;
   Counter[buzzer]--; 
  }
  if(FLAG[Rs485RcvFlag]!=0)
  {
    rs485ReadFailCount++;
    if((rs485ReadFailCount>100)&&(FLAG[SocoReadFail]!=1)&&(BigFlag[NtfyFlag]!=2))
    {
      BigFlag[NtfyFlag] = 2; FLAG[SocoReadFail]=1; Counter[buzzer]=5;
      FLAG[WFlag] =1; rs485ReadFailCount=0;
      __bic_SR_register_on_exit(LPM0_bits);
    }
  }
  if(BigFlag[SocomecTxPara]==1)         //Number of Socomec address read Parameter
    SocomecCount++;
  else
    SocomecCount=0;
  if(SocomecCount>5)                    //Parameter doesnt change for more than 3sec make it default.
   BigFlag[SocomecTxPara]=5;  
  if(FLAG[DollarStart]== true)          // $ Count
    DollarCount++;
  if(FLAG[WifiBlink]==1)
    P1OUT ^= BIT7;
  if(FLAG[WifiConnect]==1)
  {
    resetCount=0;  psWdtCount++; 
  }
  else
  {
    resetCount++;
    if(resetCount>120) 
    {
      FLAG[RstFlag]= 1; resetCount=0;
    }
    else {  FLAG[RstFlag]=0;}
  }
  if((psWdtCount>20)&&(FLAG[PsWdtFlag]!=1))
  {
    FLAG[WFlag] =1;  psWdtCount=0; psCount++;
  }
  if(psCount>4)
  {
    FLAG[RstFlag]=1; 
    psCount=0;FLAG[WifiConnect]=0;
  }
  if(FLAG[PsWdtFlag]==1)
  {
    psCount=0;psWdtCount=0;
  }
  if((wdt_time>30))
  {
    wdt_time=0; SocomecCount=0;
    BigFlag[SocomecTxPara]=0;matchCount = 0;
    __bic_SR_register_on_exit(LPM0_bits);
  }
  if(wd_count >850)
  {
    wd_count=0;
    if( BigFlag[NtfyFlag] ==1)  Counter[buzzer] =7;
    else if( BigFlag[NtfyFlag] ==2)  Counter[buzzer] =5;
    else if( BigFlag[NtfyFlag] ==3)  Counter[buzzer] =3;
    PublishEnergy(); 
  }
}