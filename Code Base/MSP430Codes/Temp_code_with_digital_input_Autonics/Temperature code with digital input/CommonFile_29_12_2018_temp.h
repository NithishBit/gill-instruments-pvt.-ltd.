//---------------------------FLAG[15]------------------------------------------
#define FramSlaveIDFlag 0  
#define FramActiveFlag  1
#define WifiConnect 2
#define WFlag 3
#define RstFlag 4
#define PsWdtFlag 5 
#define Rs485RcvFlag 6
#define OneTimeRead 7
#define WifiBlink 8
#define DollarStart 10
#define SocoReadFail 11
#define SocoSlaveMatch 12

//----------------------------BigFlag[5]-------------------------------------------
#define NtfyFlag 0
#define SocomecTxPara 1
//------------------------------------------------------------------------------

//----------------------------{Counter[10]}---------------------------------------------
#define buzzer 0       //Buzzer Counter


//------------------------------------------------------------------------------
#define FRAM_TEST_START 0x1800
#define FRAM_ACTIVE 0x1840
#define FRAM_LOW_ALERT 0x1860
#define FRAM_HIGH_ALERT 0x1880
#pragma location=0x2000
__no_init char RX_BUFFER[50];

//------------------------------------------------------------------------------
void Init_GPIO();
void Board_Init();
void delay(int);
void Delay_3X(void);
void Delay(void);
void ResetWifi(void);
void Tx_Uart(char * str);
void socomec_transmit_rs485();
void power_up();
void BuffConversion(long long int n);
void FramSlaveIDWrite(void);
void PublishTemperature(void);
void conversion(void);
int crc16(char *ptr, int count);
void writeActive (unsigned long licActive);
void writeAlert (unsigned long lowAlert,unsigned long highAlert);

//------------------------------------------------------------------------------
int crc16(char *ptr, int count)
{
  unsigned int crc;
  char i;
  crc = 0xffff;
  while (--count >= 0)
  {
    crc = crc ^ (unsigned int) *ptr++;
    i = 8;
    do
    {
      if (crc & 0x0001)
        crc = (crc >> 1) ^ 0xA001;
      else
        crc = (crc >> 1);
    } while(--i);
  }
  return (crc);
}
//------------------------------------------------------------------------------
void power_up()
{
 P1OUT|= (BIT0+BIT6+BIT7);
 __delay_cycles(100000);
 P1OUT&=~(BIT0+BIT6+BIT7);
}
//------------------------------------------------------------------------------
void Tx_Uart(char *str)
{
  int i = 0;
  for(i = 0; i < strlen(str); i++)
  {
    while (!(UCA1IFG&UCTXIFG));
    UCA1TXBUF = str[i];
  }
}

//______________________________________________________________________________
void Board_Init()
{
  // Configure UART1 9600
  UCA1CTLW0 |= UCSWRST;
  UCA1CTLW0 |= UCSSEL__SMCLK;
  UCA1CTLW0 |= UCMODE_1;
  UCA1BR0 = 52;
  UCA1BR1 = 0x00;
  UCA1MCTLW = 0x4900 | UCOS16 | UCBRF_1;
  UCA1CTLW0 |=  UCTXADDR;
  UCA1CTLW0 &= ~UCSWRST;                    // Initialize eUSCI
 //______________________________________________________________________________
// Configure UART0
  UCA0CTLW0 |= UCSWRST;                        // Baud Rate calculation
  UCA0CTLW0 |= UCSSEL__SMCLK;                  // 8000000/(16*9600) = 52.083
  UCA0CTLW0 |= UCMODE_1;                        // Fractional portion = 0.083
  UCA0CTLW0 |= UCSPB;
  UCA0BR0 = 52;                             // User's Guide Table 14-4: UCBRSx = 0x49
  UCA0BR1 = 0x00;                            // UCBRFx = int ( (52.083-52)*16) = 1
  UCA0MCTLW = 0x4900 | UCOS16 | UCBRF_1;   // 8000000/16/9600
  UCA0CTLW0 |=  UCTXADDR;
  UCA0CTLW0 &= ~UCSWRST;                    // Initialize eUSCI
  UCA0IE |= UCRXIE;                         // Enable USCI_A0 RX interrupt
  UCA1IE |= UCRXIE;
__delay_cycles(400);                      // Delay for reference settling

  __bis_SR_register(SCG0);                   // disable FLL
  CSCTL3 |= SELREF__REFOCLK;                 // Set REFO as FLL reference source
  CSCTL0 = 0;                                // clear DCO and MOD registers
  CSCTL1 &= ~(DCORSEL_7);                    // Clear DCO frequency select bits first
  CSCTL1 |= DCORSEL_3;                       // Set DCO = 8MHz
  CSCTL2 = FLLD_0 + 243;                     // DCODIV = 8MHz
  __delay_cycles(3);

  __bic_SR_register(SCG0);                   // enable FLL
  while(CSCTL7 & (FLLUNLOCK0 | FLLUNLOCK1)); // Poll until FLL is locked
  CSCTL4 = SELMS__DCOCLKDIV | SELA__REFOCLK; // set default REFO(~32768Hz) as ACLK source, ACLK = 32768Hz
}
//---------------------------- Delay -------------------------------------------
void Delay(void)
{
  for(long int i=65500;i>0;(i--))
  {
    __delay_cycles(1);
  }
}

void Delay_3X(void)
{
  Delay();
  Delay();
  Delay();
  Delay();
}
//------------------------------------------------------------------------------
void Init_GPIO()
{
  P1DIR = 0xFF; P2DIR = 0xFF; P3DIR = 0xFF;
  P1REN = 0xFF; P2REN = 0xFF; P3REN = 0xFF;
  P1OUT = 0x00; P2OUT = 0x00; P3OUT = 0x00;
  P1DIR &= ~BIT1;                         // Set P1.1 as input
  P2DIR |=  BIT2 ;       // P2.2-> DE,P2.2 -> RE Output
  P2OUT = 0;
  P2OUT |= BIT3;                                                                // Configure P2.3 as pulled-up
  P2REN |= BIT3;                                                                // P2.3 pull-up register enable
  P2IES |= BIT3;                                                                // P2.3 Hi/Low edge
  P2IE |= BIT3;                                                                 // P2.3 interrupt enabled
  // Configure UART pins
  P1SEL0 |= BIT4 | BIT5;                    // set 2-UART pin as second function
  P2SEL0 |= BIT5 | BIT6;                    // set 2-UART pin as second function
}
//------------------------------------------------------------------------------
