//******************************************************************************
//Temperature---RS485---modbus--protocol for Tk4s meter
//2 Word reading of Tk4s(Autonics)
//Restart ESP32 
//                MSP430FR2633
//             -----------------
//         /|\|             P1.0|-->LED/D5(Relay)
//          | |             P1.1|<-- TA0.CCI1A/CAP1/Digital input
//          | |     P1.4/UCA0TXD|----> [RS485](A)---->Tk4s(Autonics)[reg:0x03E8]
//          | |     P1.5/UCA0RXD|<---- [RS485](B)---->Tk4s(Autonics)
//          --|RST          P1.6|-->LED/D2 (TX)  
//            |             P1.7|-->LED/D3 (cloud) 
//            |             P2.3|<--SW
//            |             P2.4|-->SW_LED 
//            |             P3.0|-->Relay 
//            |             P3.1|-->EN/ESP32  
//            |       (RXD1)P2.5|<--TXD0/ESP32
//            |       (TXD1)P2.6|-->RXD0/ESP32 
//            |_________________|
//  Pradhan V
//  Gill Instruments.
//  29 Dec 2018
//  Built with IAR Embedded Workbench v6.50
//******************************************************************************
#include <msp430fr2632.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include "CommonFile_29_12_2018_temp.h"

                //slave_id 16,read reg 04,Temperature reg 0x03E8,Reg length 0x02//
char Temperature[8] = {0X10,0X04,0X03,0X0E8,0X00,0X02};                 //Total  Temperature 
//char Power_factor[8] ={0X10,0X04,0X03,0X0E8,0X00,0X02};            //Total Positive Active Temperature (UNIT:kWh)
char Temperature_Two[8] ={0X10,0X04,0X03,0X0E8,0X00,0X02};              //Active Power phase 1 +/- : P1(UNIT: kW/100(Signed))
char Temperature_buf[40],uart1RxBuff[60];
char slave=0,levelAlertBuff=0;
long long int digi[3];
uint32_t num;
long double Actual_Temperature=0.0,FreshTemperature=0.0;
volatile unsigned int e1Counter=0,psWdtCount=0,psCount=0,crc_out=0,DollarCount=0,Tk4sCount=0;
unsigned int rs485ReadFailCount=0,resetCount=0,rxBuffIndex=0,delay_cnt=0;
unsigned int RX_index =0,inter=0,TX_index = 0,wd_count=0,wdt_time=0;
int matchCount = 0;
volatile unsigned long *FRAM_write_ptr;
volatile unsigned long *Flash_ptrACTIVE;   // Harware Lic Active
volatile unsigned long *Flash_ptrLOW;      // Low Alert Value
volatile unsigned long *Flash_ptrHIGH;     // High Alert Value 
volatile unsigned long data;
unsigned long licActive=0,highAlertRx=0,highAlert=0,lowAlert=0,lowAlertRx=0;

bool FLAG[15] = {0};
bool Digital_Input = 0;
volatile short int BigFlag[5]= {0};
volatile unsigned int Counter[10] = {0};
//==============================================================================
void main(void)
{
  WDTCTL = WDT_ADLY_1000;                   // WDT 32ms, SMCLK, interval timer
  Init_GPIO();                               // Configure GPIO
  PM5CTL0 &= ~LOCKLPM5;                      // Disable the GPIO power-on default high-impedance mode
  Board_Init();
  power_up();                               
  ResetWifi();
   
  FRAM_write_ptr = (unsigned long *)FRAM_TEST_START;
  Flash_ptrHIGH= (unsigned long *)FRAM_HIGH_ALERT;
  Flash_ptrLOW = (unsigned long *)FRAM_LOW_ALERT; 
  Flash_ptrACTIVE= (unsigned long *)FRAM_ACTIVE;
  
  SFRIE1 |= WDTIE; 
//------------------------------------------------------------------------------
  slave=*FRAM_write_ptr ;
  licActive=*Flash_ptrACTIVE;
  highAlertRx=highAlert=*Flash_ptrHIGH;  
  lowAlertRx=lowAlert=*Flash_ptrLOW;
  P2IFG &= ~BIT3;                         // Clear P2.3 IFG
  while(1)
  {
    if (P1IN & BIT1)
        Digital_Input = true;                 // if P1.1 set, set Digital_Input
    else
        Digital_Input = false;                     // else reset
    
    if((BigFlag[SocomecTxPara]<3))
    {
     __delay_cycles(900000);
    socomec_transmit_rs485();
    __bis_SR_register(LPM0_bits|GIE);     // Enter LPM0
    __no_operation();
    __delay_cycles(900000);
    } 
    else{
    if((FLAG[WFlag]==1))
    {
      FLAG[WFlag]=0;wd_count=0;            //Buzzer();
      PublishTemperature();
     }
    if(FLAG[FramActiveFlag]==1)
    {
     writeActive(licActive);  
     FLAG[FramActiveFlag]=0;
    }
    if( FLAG[FramSlaveIDFlag] ==1)
    {   FLAG[FramSlaveIDFlag]=0;
     FramSlaveIDWrite();
    }
    if((highAlertRx!=highAlert)||((highAlertRx<99)&&(lowAlertRx>0))||(lowAlertRx!=lowAlert))
    {
      highAlert=highAlertRx;
      lowAlert=lowAlertRx;
      writeAlert(lowAlert,highAlert);   
    }
    if((FLAG[RstFlag]==1)||((e1Counter>45)&&(e1Counter<65)))
    {
     ResetWifi(); 
     FLAG[RstFlag]=0;
    }
    __bis_SR_register(LPM0_bits|GIE);     // Enter LPM0
    __no_operation();
   }
  }
  //return 0;
}
//______________________________________________________________________________
void ResetWifi(void)
{ 
  Delay(); 
  P3OUT |= BIT1;
  Delay(); 
  P3OUT &= ~BIT1;
  Delay(); 
  P3OUT |= BIT1;
  P1OUT&=~BIT7;
}
//______________________________________________________________________________
void conversion(void)
{ int conversionFlag=0;
  BuffConversion(25);
  num = digi[2];
  FreshTemperature=(((float)num))/10;
  
  if(((FreshTemperature -0.50)>Actual_Temperature)||((FreshTemperature +0.50)<Actual_Temperature))  // if(+/-)0.5 Degree celsius change in temperature  push data
    conversionFlag =1;
    Actual_Temperature=FreshTemperature;

 if(Actual_Temperature>highAlert)
  {
    BigFlag[NtfyFlag] = 1; 
  }
 else if(Actual_Temperature<lowAlert)
   BigFlag[NtfyFlag] = 2;
 else BigFlag[NtfyFlag] = 0;
 
 if(conversionFlag==1)
 { PublishTemperature();conversionFlag=0;}
}
//______________________________________________________________________________
void PublishTemperature(void)
{
  if((FLAG[WifiConnect]==1)&&(FLAG[OneTimeRead]==1))
{
  P1OUT |= BIT6;
  FLAG[PsWdtFlag]=0;
  snprintf(Temperature_buf,30,"$%4.1Lf;%d;%d;0;%ld;%ld;!",Actual_Temperature,BigFlag[NtfyFlag],Digital_Input,lowAlert,highAlert);        //$XX;Y;Z;@!  modeFlag ntfy_flag
  if(licActive!=1)Tx_Uart(Temperature_buf); //$Actual_Temperature;notification;Digital_Input;swt;lowAlert;highAlert;!
  P1OUT &= ~BIT6; psWdtCount=0;                 
   }
  else if(FLAG[WifiConnect]==1)
  {FLAG[PsWdtFlag]=0;
   snprintf(Temperature_buf,15,"$ ;%d;%d;0;%ld;%ld;!",BigFlag[NtfyFlag],Digital_Input,lowAlert,highAlert);        //XX;Y;Z;@  modeFlag ntfy_flag
  if(licActive!=1)Tx_Uart(Temperature_buf); 
  psWdtCount=0;         
  }
}
//______________________________________________________________________________
void BuffConversion(long long int n)
{
  digi[0]=((((long long int)RX_BUFFER[n-2]))*256);       //16^2=256 MSB FF
  digi[1]=((((long long int)RX_BUFFER[n-1]))*1);         //16^0=1   LSB FF
  digi[2]=(long long int)(digi[1]+digi[0]);              //16bit--FFFF to decimal conversion
}
//------------------------------------------------------------------------------
void FramSlaveIDWrite (void)
{ BigFlag[SocomecTxPara]=0;
  SYSCFG0 = FRWPPW | PFWP;
  {
    *FRAM_write_ptr=data;
  }
  SYSCFG0 = FRWPPW | PFWP | DFWP;
}
void writeActive (unsigned long licActive)
{
  SYSCFG0 = FRWPPW | PFWP;
  *Flash_ptrACTIVE= licActive;                   // Write value to flash
  SYSCFG0 = FRWPPW | PFWP | DFWP;
 }
void writeAlert (unsigned long lowAlert,unsigned long highAlert)
{
  SYSCFG0 = FRWPPW | PFWP;
  *Flash_ptrLOW= lowAlert;                   // Write value to flash
  *Flash_ptrHIGH = highAlert;                   // Write value to flash
  SYSCFG0 = FRWPPW | PFWP | DFWP;
}
//------------------------------------------------------------------------------
void socomec_transmit_rs485()
{
  Temperature[0]=Temperature_Two[0]=slave; //Volt[0]=Current[0]=
  //Delay();
  switch(BigFlag[SocomecTxPara])
  {
  case 0:
  TX_index=0;RX_index=0;P2OUT|= BIT2;                  //P2.0 -> DE (high) transmit
  P1OUT |=BIT0;  BigFlag[SocomecTxPara]=2;inter=9; FLAG[Rs485RcvFlag]=1;//rs485ReadFailCount =0;
  crc_out=crc16(Temperature,6);
  Temperature[6]=crc_out;
  Temperature[7]=(crc_out/256);
  while(TX_index < 8)
  {
    UCA0TXBUF = Temperature[TX_index++];     //{"slave id,read register,register address,number of words to read,CRC"}
     while(!(UCA0IFG & UCTXIFG));
  }
  __delay_cycles(7000);P2OUT  &= ~BIT2;//P2.0 -> DE (low)
  break;

//----------------- End of case:0-----------------------------------------------
 
  case 2:
  TX_index=0;RX_index=20;P2OUT |= BIT2; BigFlag[SocomecTxPara]=3;inter=29; //P2.0 -> DE (high) transmit

  crc_out=crc16(Temperature_Two,6);
  Temperature_Two[6]=crc_out;
  Temperature_Two[7]=(crc_out/256);

  while(TX_index < 8)
  {
    UCA0TXBUF = Temperature_Two[TX_index++];
    while(!(UCA0IFG & UCTXIFG));
  }
  __delay_cycles(7000);P2OUT  &= ~BIT2; P1OUT &=~ BIT0;//P2.0 -> DE (low)
  break;
//----------------- End of case:2-----------------------------------------------
  default: break;
  }
}

//______________________________________________________________________________
#pragma vector =USCI_A0_VECTOR //USCI_A0_VECTOR (RS-485 Recive Int)
__interrupt void USCI_A0_ISR(void)
{
   __bic_SR_register(GIE);   
  switch(__even_in_range(UCA0IV,USCI_UART_UCTXCPTIFG))
  {
  case USCI_NONE: break;
  case USCI_UART_UCRXIFG:
   
    RX_BUFFER[RX_index++]=UCA0RXBUF;    //index overflow
    if((UCA0RXBUF==04)&&(RX_BUFFER[RX_index-2]==slave))
      FLAG[SocoSlaveMatch]=true;
   if((FLAG[Rs485RcvFlag]==1)&&(FLAG[SocoSlaveMatch]==true))
     matchCount++;
   /*
    if(matchCount==7)
    {
      FLAG[OneTimeRead]=1; FLAG[Rs485RcvFlag]=0;conversion(); 
      rs485ReadFailCount=0;FLAG[SocoReadFail]=0;FLAG[SocoSlaveMatch]= false;
      __bic_SR_register_on_exit(LPM0_bits);
    } */
    if(RX_index>30)
    {
      RX_index=0; 
    }
    if((inter==matchCount)&&(inter!=0))
    {   P2OUT |= BIT2;
    rs485ReadFailCount=0;FLAG[SocoSlaveMatch]= false;matchCount++;
     conversion(); FLAG[OneTimeRead]=1; 
      __bic_SR_register_on_exit(LPM0_bits);
    }
    break;
  case USCI_UART_UCTXIFG: break;
  case USCI_UART_UCSTTIFG: break;
  case USCI_UART_UCTXCPTIFG: break;
  }
    __bis_SR_register(GIE);
  
}

//______________________________________________________________________________
#pragma vector=USCI_A1_VECTOR  //(ESP32 Recive Int)
__interrupt void USCI_UART_UCRXISR(void)
{
  if(UCA1RXBUF=='$')
  {
  FLAG[DollarStart]= true; DollarCount=0;
  }
  if(FLAG[DollarStart]== true)
  {
  uart1RxBuff[rxBuffIndex]=UCA1RXBUF;
  rxBuffIndex++;  
  }
  if((UCA1RXBUF=='!')&&(DollarCount<2))
  {
  FLAG[DollarStart]= false; 

  if((uart1RxBuff[rxBuffIndex-2])=='W')           //W---> for getting new reading(setting parameters for freash value) of energy meter
   { rxBuffIndex=0;    
     FLAG[WFlag]=1; 
      }
  else if(((uart1RxBuff[rxBuffIndex-2])=='#')&&((uart1RxBuff[rxBuffIndex-3])=='^')&&(rxBuffIndex>1))      //$28^#!----> for setting new slave id
   {   
     FLAG[FramSlaveIDFlag]=1;                                                                     // 54321
     slave=uart1RxBuff[rxBuffIndex-5];
     slave=((slave-'0')*10)+((uart1RxBuff[rxBuffIndex-4])-'0');
     data = slave;
     rxBuffIndex=0;BigFlag[SocomecTxPara]=5;
   }
  else if(((uart1RxBuff[rxBuffIndex-2])=='0')&&((uart1RxBuff[rxBuffIndex-3])=='E')&&(rxBuffIndex>1))               
  {                                                                           //$E0!----> WiFi Connection is good        
    P1OUT|= BIT7; rxBuffIndex=0;  FLAG[WifiBlink]=0; e1Counter=70;            // 321
    FLAG[WifiConnect]=1;FLAG[WFlag]=1; resetCount=0;                                           
  }
  else if(((uart1RxBuff[rxBuffIndex-2])=='2')&&((uart1RxBuff[rxBuffIndex-3])=='E')&&(rxBuffIndex>1))                    
  {                                                                                   //$E2!----> WiFi Connection is not good
    P1OUT&=~BIT7; rxBuffIndex=0; FLAG[WifiBlink]=0;e1Counter=0; FLAG[WifiConnect]=0;  // 321
  }
    else if(((uart1RxBuff[rxBuffIndex-2])=='1')&&((uart1RxBuff[rxBuffIndex-3])=='E')&&(rxBuffIndex>1))                    //E1----> Aws Connection is not good
    { 
      FLAG[WifiBlink]=1; rxBuffIndex=0; FLAG[WifiConnect]=0; e1Counter=0;
    }
   else if(((uart1RxBuff[rxBuffIndex-2])=='S')&&((uart1RxBuff[rxBuffIndex-3])=='P')&&(rxBuffIndex>1))                    //PS                 
    {   
     FLAG[PsWdtFlag]=1; rxBuffIndex=0;    
    }
    else if(((uart1RxBuff[rxBuffIndex-2])=='T')&&((uart1RxBuff[rxBuffIndex-3])=='S')&&((uart1RxBuff[rxBuffIndex-4])=='R')&&(rxBuffIndex>2))
    {                                                                           
     FLAG[RstFlag]=1; rxBuffIndex=0;                               
    }
    else if(((uart1RxBuff[rxBuffIndex-2])=='N')&&((uart1RxBuff[rxBuffIndex-3])=='O')&&((uart1RxBuff[rxBuffIndex-4])=='C')&&(rxBuffIndex>2))
    {                                                                           //CON
      FLAG[FramActiveFlag] =1;  rxBuffIndex=0; licActive=0;                                //321
    }
    else if(((uart1RxBuff[rxBuffIndex-2])=='F')&&((uart1RxBuff[rxBuffIndex-3])=='F')&&((uart1RxBuff[rxBuffIndex-4])=='O')&&((uart1RxBuff[rxBuffIndex-4])=='C')&&(rxBuffIndex>3))
    {
      FLAG[FramActiveFlag] =1;  rxBuffIndex=0; licActive=1;                                //COFF
    }                                                                            //4321
    if((UCA1RXBUF=='!')&&((uart1RxBuff[rxBuffIndex-7])=='H')&&((uart1RxBuff[rxBuffIndex-8])=='.')&&((uart1RxBuff[rxBuffIndex-13])=='L')&&(rxBuffIndex>11))           
    {  
    levelAlertBuff=0;                                                           //L10.0.H90.0.!
    levelAlertBuff=((uart1RxBuff[rxBuffIndex-6])-'0');                          //3210987654321
    levelAlertBuff=((levelAlertBuff)*10)+((uart1RxBuff[rxBuffIndex-5])-'0');    
    highAlertRx=levelAlertBuff; levelAlertBuff=0;                                                  
        levelAlertBuff=uart1RxBuff[rxBuffIndex-12];
    levelAlertBuff=((levelAlertBuff-'0')*10)+((uart1RxBuff[rxBuffIndex-11])-'0');
    lowAlertRx=levelAlertBuff; rxBuffIndex=0; 
    BigFlag[SocomecTxPara]=5;  // for not entering socomec loop
    }
  
if(rxBuffIndex>55)
 {
  rxBuffIndex=0;  
 }
 __bic_SR_register_on_exit(LPM0_bits);  
 }  
}
//------------------------------------------------------------------------------
// Port 2 interrupt service routine (P2.3)
#pragma vector=PORT2_VECTOR
__interrupt void Port_2(void)
{
  Counter[buzzer]=0;BigFlag[NtfyFlag]=0;
  P3OUT &= ~BIT0;                         // Relay P3.0 OFF
  P3OUT &= ~BIT2;                         // Buzzer P3.2 OFF
  P2IFG &= ~BIT3;                         // Clear P2.3 IFG
}
//______________________________________________________________________________
// Watchdog Timer interrupt service routine
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=WDT_VECTOR
__interrupt void WDT_ISR(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(WDT_VECTOR))) WDT_ISR (void)
#else
#error Compiler not supported!
#endif
{ 
  wdt_time++;  wd_count++;e1Counter++;
  if(Counter[buzzer]>1)
  {
    P3OUT ^= BIT2;
   Counter[buzzer]--; 
  }
  if(FLAG[Rs485RcvFlag]!=0)
  {
    rs485ReadFailCount++;
    if((rs485ReadFailCount>100)&&(FLAG[SocoReadFail]!=1))
    {
      BigFlag[NtfyFlag] = 3; FLAG[SocoReadFail]=1; Counter[buzzer]=5;
      FLAG[WFlag] =1; rs485ReadFailCount=0;
      __bic_SR_register_on_exit(LPM0_bits);
    }
  }
  if(BigFlag[SocomecTxPara]==1)         //Number of Socomec address read Parameter
    Tk4sCount++;
  if(Tk4sCount>3)                    //Parameter doesnt change for more than 3sec make it default.
    BigFlag[SocomecTxPara]=5;  
  if(FLAG[DollarStart]== true)          // $ Count
    DollarCount++;
  if(FLAG[WifiBlink]==1)
    P1OUT ^= BIT7;
  if(FLAG[WifiConnect]==1)
  {
    resetCount=0;  psWdtCount++; 
  }
  else
  {
    resetCount++;
    if(resetCount>120) 
    {
      FLAG[RstFlag]= 1; resetCount=0;
    }
    else {  FLAG[RstFlag]=0;}
  }
  if((psWdtCount>20)&&(FLAG[PsWdtFlag]!=1))
  {
    FLAG[WFlag] =1;  psWdtCount=0; psCount++;
  }
  if(psCount>3)
  {
    FLAG[RstFlag]=1; 
    psCount=0;FLAG[WifiConnect]=0;
  }
  if(FLAG[PsWdtFlag]==1)
  {
    psCount=0;psWdtCount=0;
  }
  if((wdt_time>9))
  {
    wdt_time=0; Tk4sCount=0; matchCount = 0;
    BigFlag[SocomecTxPara]=0;
    __bic_SR_register_on_exit(LPM0_bits);
  }
  if(wd_count >850)
  {
    wd_count=0;
    if( BigFlag[NtfyFlag] ==1)  Counter[buzzer] =7;
    if( BigFlag[NtfyFlag] ==3)  Counter[buzzer] =5;
    PublishTemperature(); 
  }
}