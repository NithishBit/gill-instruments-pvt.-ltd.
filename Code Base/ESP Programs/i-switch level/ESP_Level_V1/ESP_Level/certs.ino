//***************************************  LEVEL SENSE  **********************************************************************
// Company Name:Gill Instruments Bangalore
// Author: Rahul 
// Date: 15 June 2018
// Application: Iswitch (PCB Ver -I-SWITCH32 P314726) (IoT based Level Transmitter) 
//------------------------------------------------------------------------------------------------------------
//        ESP32 Transmits following bytes to MSP430FR2632 via UART0 
//       "$E2!"     - NO Wi-FI
//       "$E1"       - Wi-Fi connected 
//       "$E0"       - conneced to Server (All good)
//       "$W"        - health Status of Iswitch (query from Server)( Iswitch will reply with Data)
//       "$L10.H90!" - set low and high in MSP
//       "S01"      - to update switch flag in MSP
//       "PS"       - to acknowlegde that it has succesfully published
//************************************************************************************************************
//        MSP to send data in the following format
//        
//                  XX;Y;Z;L;H;@
//
//        Where   XX-Level
//                 Y-Notification
//                 Z-Switch status
//                 L-Low
//                 H-High
//************************************************************************************************************
//        Final JSON sent to server is
//        {
//            "type": "0",
//            "data": "XX",
//            "ntfy": "Y",
//            "swt": "Z",
//            "low": "L",
//            "high":"H";
//          }
//************************************************************************************************************
//-------------------------------------------TOPICS----------------------------------------------------------------
char deviceid[]="Iswitch_A009";
char topic_pub[]="Gill/A009/value";
char topic_sub[]="Gill/A009/status";
char topic_notify[]="Gill/A009/notify";
//-----------------------------------------------------------------------------------------------------------------
#include <WiFi.h>
#include <WiFiClient.h>
#include <WebServer.h>
#include <DNSServer.h>
#include <Preferences.h>
#include <WiFiClientSecure.h>
#include <PubSubClient.h>
#include<time.h>
#include<stdio.h>
#include <ArduinoJson.h>
//-----------------------------------------------------------------------------------------------------------------
WebServer server(80);
WiFiClientSecure wiFiClient;
const char *softAP_ssid = "ISWITCH_A009";
const char *softAP_password = "12345678";
char ssid[32] = "";
char password[32] = "";
Preferences preferences,pref;
/* Soft AP network parameters */
IPAddress apIP(192, 168, 4, 1);
IPAddress netMsk(255, 255, 255, 0);
//-----------------------------------------------------------------------------------------------------------------
/** Should I connect to WLAN asap? */
boolean connect;
/** Last time I tried to connect to WLAN */
long lastConnectTry = 0;
/** Current WLAN status */
int status = WL_IDLE_STATUS;
bool sta=false;
bool ap=false;
char payload[256];
uint8_t wifiattempts=0;
long connec=0,wifistatus=0;
int s;
char buf4[129];
//-----------------------------------------------------------------------------------------------------------------
void msgReceived(char* topic, byte* payload, unsigned int len);
const char* awsEndpoint = "a3gsc2swtcbfsp.iot.us-east-1.amazonaws.com";
PubSubClient pubSubClient(awsEndpoint, 8883, msgReceived, wiFiClient); 

//------------------------------------------------- Setup ---------------------------------------------------------------
  void setup() 
{
  WiFi.mode(WIFI_STA);
  delay(500);
  Serial.begin(9600);
  Serial.print("$E2!");
  preferences.begin("CapPortAdv", false);
  loadCredentials();
  connect = strlen(ssid) > 0;
  if(connect)
   sta=connectWifi();
   delay(500);
  if(!sta){
    ap=true;
    Serial.print("$E2!");
  WiFi.softAPConfig(apIP, apIP, netMsk);
  WiFi.softAP(softAP_ssid, softAP_password);
  delay(500);
  server.begin();
  }
  else connect=false;
  server.on("/", handleWifi);
  server.on("/wifi", handleWifi);
  server.on("/wifisave", handleWifiSave);
  server.on("/generate_204", handleRoot);  //Android captive portal. Maybe not needed. Might be handled by notFound handler.
  server.on("/fwlink", handleRoot);  //Microsoft captive portal. Maybe not needed. Might be handled by notFound handler.
  server.onNotFound ( handleNotFound );
  set_cert();
}
//================================= end of setup ====================================================================================

uint8_t connectWifi() {
// Serial.println("Connecting as wifi client...");
  WiFi.begin(ssid, password);
  delay(500);
  int connRes = WiFi.waitForConnectResult();
  if(connRes==3)
   return 1;
  else if(connRes==1)
   return 2;
  else
   return 0;
}

//------------------------------------------------ Main loop Starts Here  ------------------------------------------------------------
void loop() {
  if(wifiattempts>15){
     ESP.restart();
  }
if(connect&&millis()-connec>30000)
{   if(!ap)
      startap();
    delay(500);
    uint8_t conwi=connectWifi();
    if(conwi==0){
    wifiattempts++;
    connect=true;
    Serial.print("$E2!");
    }
    else if(conwi==1)
    { connect =false;
      wifiattempts=0;
      sta=true;
      Serial.print("$E1!");
    }
    lastConnectTry = millis();
    connec=millis();
}
  if(sta&&ap)
  {
    if(millis()-lastConnectTry>120000)
     {
       WiFi.mode(WIFI_STA);
       lastConnectTry = millis();
       ap=false;
       server.stop();
       preferences.end();
     }
  }
if(millis()-wifistatus>30000)
  {
    status = WiFi.status();
      if (status != WL_CONNECTED) {
       connect=true;
      }
    wifistatus=millis();
  }  
  //---Do work---------:
if(sta&&status==WL_CONNECTED){
  pubSubCheckConnect();
//------------------------------------------- Read level from UART -------------------------------------------------------------
 if(Serial.available()>0){
    Serial.readBytesUntil('@',buf4,128);
    char *p = buf4;
    char *str;
    StaticJsonBuffer<256> jsonBuffer;
    JsonObject& root=jsonBuffer.createObject();
    root["type"]="0";
    if((str = strtok_r(p, ";", &p)) != NULL)
    root["data"]=str;
    if((str = strtok_r(p, ";", &p)) != NULL)
    root["ntfy"]=str;
    if((str = strtok_r(p, ";", &p)) != NULL)
    root["swt"]=str;
    if((str = strtok_r(p, ";", &p)) != NULL)
    root["low"]=str;
    if((str = strtok_r(p, ";", &p)) != NULL)
    root["high"]=str;
    root.printTo(payload);
    boolean ps=pubSubClient.publish(topic_pub, payload);
    if(ps==true){
      Serial.print("$PS!");
    }
  }
}
//------------------------------------------------------------------------------------------------------------------------------
if(ap)
{server.handleClient();}
}

//================================================ Main Loop ends here ==========================================================
//----- To check if Internet is there -------
void pubSubCheckConnect() {
  if ( ! pubSubClient.connected()) {
    if( !pubSubClient.connected()) {
      Serial.print("$E1!");
      pubSubClient.connect(deviceid);
      delay(1000);
    }
    if(pubSubClient.connected()){
   // Serial.println("AWS connected");
    pubSubClient.subscribe(topic_sub);
    pubSubClient.subscribe(topic_notify);
    Serial.print("$E0!");
    }
  }
  pubSubClient.loop();
}
//--------------------------------------------------------------------------------------------------------------------------------
// ------------------- Receive messagefrom Server ----------------------
void msgReceived(char* topic, byte* payload, unsigned int length) {
  if(strcmp(topic,topic_sub)==0){
    Serial.print("$");
    for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);                // send W to UART0
   }
   Serial.print("!");
  }
  if(strcmp(topic,topic_notify)==0){               // if data from notify parse json
    StaticJsonBuffer<250> jsonBuffer;
    JsonObject& root = jsonBuffer.parseObject(payload);
    if (root.success()) {
    const char* lo = root["low"];
    const char* hig = root["high"];
    const char* ssw = root["sw"];
    Serial.print("$");
    Serial.print(ssw);
    Serial.print("!");
    if(lo){
    Serial.print("$");  
    Serial.print("L");
    Serial.print(lo);
    Serial.print(".");
    Serial.print("H");
    Serial.print(hig);
    Serial.print(".");
    Serial.print("!");
    }
    }
  }
}

//--------------------------------------------------------------------------------------------------------------------------------
//---------------------- when there is No wifi start AP mode -------------
void startap(){
  ap=true;
    WiFi.mode(WIFI_AP);
    delay(5000);        //todo deep sleep
    WiFi.mode(WIFI_AP_STA);
   server.begin();
   Serial.print("$E2!");
   //Serial.println("server Started");
}
//-------------------------------------------------------------------------------------------------------------------------------
// Load WLAN credentials from flash Memory

  void loadCredentials() {
  preferences.begin("CapPortAdv", false);
  preferences.getString("ssid", ssid, sizeof(ssid));
  preferences.getString("password", password, sizeof(password));
  preferences.end();
}
//------------------------------------------------------------------------------------------------------------------------------
// Store WLAN credentials from flash Memory

  void saveCredentials() {
  preferences.begin("CapPortAdv", false);
  preferences.putString("ssid", ssid);
  preferences.putString("password", password);
  preferences.end();
}
//-----------------------------------------------------------------------------------------------------------------------------
// Handle root or redirect to captive portal */
//-----------------------------------------------------------------------------------------------------------------------------  
  void handleRoot() {
  server.sendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  server.sendHeader("Pragma", "no-cache");
  server.sendHeader("Expires", "-1");
  server.setContentLength(CONTENT_LENGTH_UNKNOWN);
  server.send(200, "text/html", ""); // Empty content inhibits Content-length header so we have to close the socket ourselves.
  server.sendContent(
    "<html><head></head><body>"
    "<h1>I-Switch!!</h1>"
  );
  server.sendContent(
    "<p>Configure your <a href='/wifi'>WiFi Connection here</a>.</p>"
    "</body></html>"
  );
  server.sendContent("");
  server.client().stop(); // Stop is needed because we sent no content length
}

/** Wifi config page handler */
void handleWifi() {
  server.sendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  server.sendHeader("Pragma", "no-cache");
  server.sendHeader("Expires", "-1");
  server.setContentLength(CONTENT_LENGTH_UNKNOWN);
  server.send(200, "text/html", ""); // Empty content inhibits Content-length header so we have to close the socket ourselves.
  server.sendContent(
    "<html><head></head><body>"
    "<h1> WWW.Gill-Instruments.COM </h1>"
    "<h2>Wifi Configuration</h2>"
  );
  
  server.sendContent(
    "\r\n<br />"
    "<table><tr><th align='left'>Current Wifi Details</th></tr>"
  );
  server.sendContent(String() + "<tr><td>SSID " + String(ssid) + "</td></tr>");
  server.sendContent(
    "</table>"
    "\r\n<br />"
    "<table><tr><th align='left'>Available Wifi (refresh if any missing)</th></tr>"
  );
  int n = WiFi.scanNetworks();
  if (n > 0) {
    for (int i = 0; i < n; i++) {
      server.sendContent(String() + "\r\n<tr><td>SSID " + WiFi.SSID(i) + String((WiFi.encryptionType(i) == WIFI_AUTH_OPEN)?" ":" *") + " (" + WiFi.RSSI(i) + ")</td></tr>");
    }
  } else {
    server.sendContent(String() + "<tr><td>No WLAN found</td></tr>");
  }
  server.sendContent(
    "</table>"
    "\r\n<br /><form method='POST' action='wifisave'><h4>Connect to network:</h4>"
    "<input type='text' placeholder='network' name='n'/>"
    "<br /><input type='password' placeholder='password' name='p'/>"
    "<br /><input type='submit' value='Connect/Disconnect'/></form>"
    "<p>You may want to <a href='/'>return to the home page</a>.</p>"
    "</body></html>"
  );
  server.sendContent("");
  server.client().stop(); // Stop is needed because we sent no content length
}

/** Handle the WLAN save form and redirect to WLAN config page again */
void handleWifiSave() {
  server.arg("n").toCharArray(ssid, sizeof(ssid) - 1);
  server.arg("p").toCharArray(password, sizeof(password) - 1);
  server.sendHeader("Location", "wifi", true);
  server.sendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  server.sendHeader("Pragma", "no-cache");
  server.sendHeader("Expires", "-1");
  server.send ( 302, "text/plain", "");  // Empty content inhibits Content-length header so we have to close the socket ourselves.
  server.client().stop(); // Stop is needed because we sent no content length
  saveCredentials();
  connect = strlen(ssid) > 0; // Request WLAN connect with new credentials if there is a SSID
}

void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += ( server.method() == HTTP_GET ) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for ( uint8_t i = 0; i < server.args(); i++ ) {
    message += " " + server.argName ( i ) + ": " + server.arg ( i ) + "\n";
  }
  server.sendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  server.sendHeader("Pragma", "no-cache");
  server.sendHeader("Expires", "-1");
  server.send ( 404, "text/plain", message );
}
//--------------------------------------------------------------------------------------------------------------------------------------