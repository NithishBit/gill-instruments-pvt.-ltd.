//***************************************  LEVEL SENSE  **********************************************************************
// Company Name:Gill Instruments Bangalore
// Author: Rahul 
// Date: 15 June 2018
// Application: Iswitch (PCB Ver -I-SWITCH32 P314726) (IoT based Level Transmitter) 
//------------------------------------------------------------------------------------------------------------
//        ESP32 Transmits following bytes to MSP430FR2632 via UART0 
//       "E2"       - NO Wi-FI
//       "E1"       - Wi-Fi connected 
//       "E0"       - conneced to Server (All good)
//       "W"        - health Status of Iswitch (query from Server)( Iswitch will reply with Data)
//       "$L10.H90.!" - set low and high in MSP
//       "S01"      - to update switch flag in MSP
//       "PS"       - to acknowlegde that it has succesfully published
//************************************************************************************************************
//        MSP to send data in the following format
//        
//                  $XX;Y;Z;L;H;!
//
//        Where   XX-Level
//                 Y-Notification
//                 Z-Switch status
//                 L-Low
//                 H-High
//************************************************************************************************************
//        Final JSON sent to server is
//        {
//            "type": "0",
//            "data": "XX",
//            "ntfy": "Y",
//            "swt": "Z",
//            "low": "L",
//            "high":"H";
//          }
//************************************************************************************************************
//-------------------------------------------TOPICS----------------------------------------------------------------
char deviceid[]="A00E-ISwitch";
char topic_pub[]="Gill/A00E/value";
char topic_sub[]="Gill/A00E/status";
char topic_notify[]="Gill/A00E/notify";
const char *softAP_ssid = "ISWITCH_A00E";
const char *softAP_password = "12345678";
//-----------------------------------------------------------------------------------------------------------------
#include <WiFi.h>
#include <WiFiClient.h>
#include <WebServer.h>
#include <DNSServer.h>
#include <Preferences.h>
#include <WiFiClientSecure.h>
#include <PubSubClient.h>
#include<time.h>
#include<stdio.h>
#include <ArduinoJson.h>
//-----------------------------------------------------------------------------------------------------------------
WebServer server(80);
WiFiClientSecure wiFiClient;
char ssid[32] = "";
char password[32] = "";
Preferences preferences,pref;
/* Soft AP network parameters */
IPAddress apIP(192, 168, 4, 1);
IPAddress netMsk(255, 255, 255, 0);
//-----------------------------------------------------------------------------------------------------------------
long lastConnectTry = 0;
long prevWifiAttempt = 0;
bool ap=false;
char payload[256];
uint8_t wifiattempts=0;
char buf4[129];
bool subscribed=false;
bool wifi_connectedu = false;
bool valid=true;
//-----------------------------------------------------------------------------------------------------------------
void msgReceived(char* topic, byte* payload, unsigned int len);
const char* awsEndpoint = "a3gsc2swtcbfsp.iot.us-east-1.amazonaws.com";
PubSubClient pubSubClient(awsEndpoint, 8883, msgReceived, wiFiClient); 

void WiFiEvent(WiFiEvent_t event)
{
    //Serial.printf("[WiFi-Callback] Event: %d\n", event);

    switch (event)
    {
    case SYSTEM_EVENT_STA_GOT_IP:
        prevWifiAttempt+=30000;
        //Serial.println("Got IP address: ");
        //Serial.println(WiFi.localIP());
        wifi_connectedu=true;
        wifiattempts=0;
        break;
    case SYSTEM_EVENT_STA_DISCONNECTED:
        Serial.print("$E2!");
        wifi_connectedu=false;
        break;
    }
}

//------------------------------------------------- Setup ---------------------------------------------------------
  void setup() 
{
  WiFi.mode(WIFI_STA);
  delay(500);
  Serial.begin(9600);
  WiFi.onEvent(WiFiEvent);
  preferences.begin("CapPortAdv", false);
  loadCredentials();
  valid = strlen(ssid) > 0;
  Serial.print("$E2!");
  connectWifi();
  WiFi.softAPConfig(apIP, apIP, netMsk);
  WiFi.softAP(softAP_ssid, softAP_password);
  delay(500);

  server.on("/", handleWifi);
  server.on("/wifi", handleWifi);
  server.on("/wifisave", handleWifiSave);
  server.on("/generate_204", handleRoot);  //Android captive portal. Maybe not needed. Might be handled by notFound handler.
  server.on("/fwlink", handleRoot);  //Microsoft captive portal. Maybe not needed. Might be handled by notFound handler.
  server.onNotFound ( handleNotFound );
  set_cert();
}
//================================= end of setup ====================================================================================

uint8_t connectWifi() {
  if(valid){
  WiFi.begin(ssid, password);
  delay(500);
  uint8_t connRes = WiFi.waitForConnectResult();
  wifiattempts++;
   return connRes;
  }
  else{
    Serial.println("No valid WiFi Details Stored");
    return 0;
  }
  
}
//------------------------------------------------ Main loop Starts Here  ------------------------------------------------------------
void loop() {
  if(wifiattempts>15){
    delay(2000);
    ESP.restart();
  }
if(wifi_connectedu){
  pubSubCheckConnect();
//------------------------------------------- Read level from UART -------------------------------------------------------------
if(Serial.read()=='$'){
    Serial.readBytesUntil('!',buf4,128);
    char *p = buf4;
    char *str;
    StaticJsonBuffer<256> jsonBuffer;
    JsonObject& root=jsonBuffer.createObject();
    root["type"]="0";
    if((str = strtok_r(p, ";", &p)) != NULL)
    root["data"]=str;
    if((str = strtok_r(p, ";", &p)) != NULL)
    root["ntfy"]=str;
    if((str = strtok_r(p, ";", &p)) != NULL)
    root["swt"]=str;
    if((str = strtok_r(p, ";", &p)) != NULL)
    root["low"]=str;
    if((str = strtok_r(p, ";", &p)) != NULL)
    root["high"]=str;
    root.printTo(payload);
    boolean ps=pubSubClient.publish(topic_pub, payload);
    if(ps==true){
      Serial.print("$PS!");
    }
  }
  if(ap){
    server.stop();
    WiFi.mode(WIFI_STA);
    ap=false;
  }
}
else        //Wifi Not Connected
{   if(!ap)
      startap();
    long now = millis();
    if (now - prevWifiAttempt > 10000) {
      prevWifiAttempt = now;
      // Attempt to Connect to Wifi
    if(connectWifi()==1){
      prevWifiAttempt+=20000;
    }
    }
}
//------------------------------------------------------------------------------------------------------------------------------
if(ap)
{server.handleClient();}
}

//================================================ Main Loop ends here ==========================================================
long lastReconnectAttempt = 0;
boolean reconnect() {
  if (pubSubClient.connect(deviceid)) {
    // Once connected, publish an announcement...
    pubSubClient.subscribe(topic_sub);
    pubSubClient.subscribe(topic_notify);
    Serial.print("$E0!");
  }
  return pubSubClient.connected();
}

//----- To check if Internet is there -------
boolean pubSubCheckConnect() {
if (!pubSubClient.connected()) {
    long now = millis();
    if (now - lastReconnectAttempt > 10000) {
      Serial.print("$E1!");
      lastReconnectAttempt = now;
      // Attempt to reconnect
      if (reconnect()) {
        lastReconnectAttempt = 0;
      }
    }
  } else {
    // Client connected
    pubSubClient.loop();
  }
}
//--------------------------------------------------------------------------------------------------------------------------------
// ------------------- Receive messagefrom Server ----------------------
void msgReceived(char* topic, byte* payload, unsigned int length) {
  if(strcmp(topic,topic_sub)==0){
    Serial.print("$");     
    for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);                // send W to UART0
   }
   Serial.print("!");
  }
  if(strcmp(topic,topic_notify)==0){               // if data from notify parse json
    StaticJsonBuffer<250> jsonBuffer;
    JsonObject& root = jsonBuffer.parseObject(payload);
    if (root.success()) {
    const char* lo = root["low"];
    const char* hig = root["high"];
    const char* ssw = root["sw"];
    Serial.print(ssw);
    if(lo){
    Serial.print("$");  
    Serial.print("L");
    Serial.print(lo);
    Serial.print(".");
    Serial.print("H");
    Serial.print(hig);
    Serial.print(".");
    Serial.print("!");
    }
    }
  }
}

//--------------------------------------------------------------------------------------------------------------------------------
//---------------------- when there is No wifi start AP mode -------------
void startap(){
    ap=true;
    WiFi.mode(WIFI_AP);
    delay(5000);        //todo deep sleep
    WiFi.mode(WIFI_AP_STA);
    server.begin();
}
//-------------------------------------------------------------------------------------------------------------------------------
// Load WLAN credentials from flash Memory

  void loadCredentials() {
  preferences.begin("CapPortAdv", false);
  preferences.getString("ssid", ssid, sizeof(ssid));
  preferences.getString("password", password, sizeof(password));
  preferences.end();
}
//------------------------------------------------------------------------------------------------------------------------------
// Store WLAN credentials from flash Memory

  void saveCredentials() {
  preferences.begin("CapPortAdv", false);
  preferences.putString("ssid", ssid);
  preferences.putString("password", password);
  preferences.end();
}
//-----------------------------------------------------------------------------------------------------------------------------
// Handle root or redirect to captive portal */
//-----------------------------------------------------------------------------------------------------------------------------  
  void handleRoot() {
  server.sendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  server.sendHeader("Pragma", "no-cache");
  server.sendHeader("Expires", "-1");
  server.setContentLength(CONTENT_LENGTH_UNKNOWN);
  server.send(200, "text/html", ""); // Empty content inhibits Content-length header so we have to close the socket ourselves.
  server.sendContent(
    "<html><head></head><body>"
    "<h1>I-Switch!!</h1>"
  );
  server.sendContent(
    "<p>Configure your <a href='/wifi'>WiFi Connection here</a>.</p>"
    "</body></html>"
  );
  server.sendContent("");
  server.client().stop(); // Stop is needed because we sent no content length
}

/** Wifi config page handler */
void handleWifi() {
  server.sendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  server.sendHeader("Pragma", "no-cache");
  server.sendHeader("Expires", "-1");
  server.setContentLength(CONTENT_LENGTH_UNKNOWN);
  server.send(200, "text/html", ""); // Empty content inhibits Content-length header so we have to close the socket ourselves.
  server.sendContent(
    "<html><head></head><body>"
    "<h1> WWW.Gill-Instruments.COM </h1>"
    "<h2>Wifi Configuration</h2>"
  );
  
  server.sendContent(
    "\r\n<br />"
    "<table><tr><th align='left'>Current Wifi Details</th></tr>"
  );
  server.sendContent(String() + "<tr><td>SSID " + String(ssid) + "</td></tr>");
  server.sendContent(
    "</table>"
    "\r\n<br />"
    "<table><tr><th align='left'>Available Wifi (refresh if any missing)</th></tr>"
  );
  int n = WiFi.scanNetworks();
  if (n > 0) {
    for (int i = 0; i < n; i++) {
      server.sendContent(String() + "\r\n<tr><td>SSID " + WiFi.SSID(i) + String((WiFi.encryptionType(i) == WIFI_AUTH_OPEN)?" ":" *") + " (" + WiFi.RSSI(i) + ")</td></tr>");
    }
  } else {
    server.sendContent(String() + "<tr><td>No WLAN found</td></tr>");
  }
  server.sendContent(
    "</table>"
    "\r\n<br /><form method='POST' action='wifisave'><h4>Connect to network:</h4>"
    "<input type='text' placeholder='network' name='n'/>"
    "<br /><input type='password' placeholder='password' name='p'/>"
    "<br /><input type='submit' value='Connect/Disconnect'/></form>"
    "<p>You may want to <a href='/'>return to the home page</a>.</p>"
    "</body></html>"
  );
  server.sendContent("");
  server.client().stop(); // Stop is needed because we sent no content length
}

/** Handle the WLAN save form and redirect to WLAN config page again */
void handleWifiSave() {
  server.arg("n").toCharArray(ssid, sizeof(ssid) - 1);
  server.arg("p").toCharArray(password, sizeof(password) - 1);
  server.sendHeader("Location", "wifi", true);
  server.sendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  server.sendHeader("Pragma", "no-cache");
  server.sendHeader("Expires", "-1");
  server.send ( 302, "text/plain", "");  // Empty content inhibits Content-length header so we have to close the socket ourselves.
  server.client().stop(); // Stop is needed because we sent no content length
  saveCredentials();
  valid = strlen(ssid) > 0; // Request WLAN connect with new credentials if there is a SSID
}

void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += ( server.method() == HTTP_GET ) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for ( uint8_t i = 0; i < server.args(); i++ ) {
    message += " " + server.argName ( i ) + ": " + server.arg ( i ) + "\n";
  }
  server.sendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  server.sendHeader("Pragma", "no-cache");
  server.sendHeader("Expires", "-1");
  server.send ( 404, "text/plain", message );
}
//--------------------------------------------------------------------------------------------------------------------------------------